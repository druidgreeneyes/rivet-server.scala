package similarity_test.document

import cats.effect.IO
import cats.syntax.show.*
import fs2.io.file.Files
import fs2.io.file.Path
import fs2.Stream
import similarity.app.document.DocumentController
import similarity.inject.*
import similarity.inject.given
import similarity.logging.log
import similarity.model.Metadata
import similarity.syntax.*
import similarity_test.api.debug
import similarity_test.api.getParams
import similarity_test.api.APISuite
import similarity_test.config.given
import sttp.capabilities.fs2.Fs2Streams
import sttp.client3.*
import sttp.model.MediaType
import sttp.model.StatusCode

class DocumentAPISuite extends APISuite("document") {

  val controller =
    ResourceSuiteLocalFixture("controller", injectResource[DocumentController])

  override def munitFixtures: Seq[Fixture[?]] = Seq(controller)

  override def controllerEndpoints = controller().Endpoints

  val textFilePath = Path("README.md").absolute
  val textFileContentType = MediaType.TextPlainUtf8
  val textFileMetadata = Metadata("fileName" -> "README.md")
  val textFile = Files.forIO.readAll(textFilePath)

  val csvFilePath = Path("src/test/resources/data/documents.csv").absolute
  val csvFileContentType = MediaType.TextCsv
  val csvFileMetadata =
    Metadata("fileName" -> "test.csv", "documentColumn" -> "comment")
  val csvFile = Files.forIO.readAll(csvFilePath)

  test("PUT /document should accept a plain text file") {
    for {
      // when
      response <- basicRequest
        .put(
          baseUri
            .addParams(getParams(textFileMetadata)*),
        )
        .contentType(textFileContentType)
        .streamBody(Fs2Streams[IO])(textFile)
        .send(backend)
      _ = log.debug(response.show())

      // then
      _ = assertSuccess(response)

      body <- decodeBody[Seq[Metadata]](response)

      _ = assertEquals(body.size, 1)
      _ = body.foreach { md =>
        assertEquals(md.get("fileName"), textFileMetadata.get("fileName"))
        assertEquals(
          md.contentType,
          Some(textFileContentType.toString),
        )
        assert(md.riv.nonEmpty)
        assert(md.id.nonEmpty)
      }
    } yield ()
  }

  test("PUT /document should accept a csv file of multiple documents") {
    for {
      // when
      response <- basicRequest
        .put(baseUri.addParams(getParams(csvFileMetadata)*))
        .contentType(csvFileContentType)
        .streamBody(Fs2Streams[IO])(csvFile)
        .debug
        .send(backend)

      // then
      _ = assertSuccess(response)

      body <- decodeBody[Seq[Metadata]](response)

      _ <- Files.forIO
        .readUtf8Lines(csvFilePath)
        .compile
        .count
        .map { lineCount =>
          log.debug(s"Recieved ${body.size} documents")
          // Assume the test file has a header row
          // Assume an empty line at the end of the file
          assertEquals[Any, Any](body.size, lineCount - 2)
        }

      _ = body.foreach { md =>
        assertEquals(md.get("fileName"), csvFileMetadata.get("fileName"))
        assertEquals(
          md.contentType,
          Some(csvFileContentType.toString),
        )
        assert(md.riv.nonEmpty)
        assert(md.id.nonEmpty)

        // These two should come from columns in the csv file
        assert(md.get("hash").nonEmpty)
        assert(md.get("comment_id").nonEmpty)
      }
    } yield ()
  }
}
