package similarity_test

import similarity.config.*
import similarity.persistence.Backend

package object config {
  given config: Config = Config(
    persistence = PersistenceConfig(
      backend = Backend.HashMap,
    ),
    env = EnvConfig(
      prefix = "RIVET",
    ),
  )
}
