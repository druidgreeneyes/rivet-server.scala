package similarity_test.ops

import cats.effect.IO
import doobie.implicits.*
import doobie.util.transactor.Transactor
import doobie.ConnectionIO
import io.circe.generic.auto.*
import io.circe.parser
import scala.concurrent.duration.*
import scala.concurrent.duration.Duration
import scala.util.Random
import similarity.app.ops.CosineSimilarityResponse
import similarity.app.ops.NearSpaceCluster
import similarity.app.ops.NearSpaceClusterResponse
import similarity.app.ops.OpsController
import similarity.inject.*
import similarity.inject.given
import similarity.persistence.sql.transactorResource
import similarity.syntax.either.*
import similarity.syntax.option.*
import similarity_test.api.debug
import similarity_test.api.APISuite
import sttp.client3.*
import sttp.model.StatusCode

class OpsAPISuite extends APISuite("ops") {

  val controller =
    ResourceSuiteLocalFixture("controller", injectResource[OpsController])

  val database =
    ResourceSuiteLocalFixture("database", injectResource[Transactor[IO]])

  override def munitFixtures: Seq[Fixture[?]] = Seq(controller, database)

  override def munitTimeout: Duration = 30.minutes

  override def controllerEndpoints = controller().Endpoints

  test(
    "POST /compare should compare two documents and return a comparison result",
  ) {
    for {
      // given
      ids <- sql"select max(id), min(id) from documents"
        .query[(Long, Long)]
        .unique
        .map { (max, min) =>
          (Random.between(min, max).toString, Random.between(min, max).toString)
        }
        .transact(database())
      (idA, idB) = ids

      // when
      response <- basicRequest
        .post(
          baseUri
            .addPath("cosine")
            .addParams("docA" -> idA, "docB" -> idB),
        )
        .send(backend)

      // then
      _ = assertSuccess(response)

      body <- decodeBody[CosineSimilarityResponse](response)
      _ = assertEquals(body.documentA.id, some(idA))
      _ = assertEquals(body.documentB.id, some(idB))
      _ = assert(
        body.value >= 0 && body.value <= 1,
        s"Illegal cosine similarity value: ${body.value}",
      )
    } yield ()
  }

  test(
    "POST /duplicates should crawl an entire corpus and return all groups of documents that are above the given similarity threshold",
  ) {
    for {
      response <- basicRequest
        .post(
          baseUri
            .addPath("duplicates")
            .addParams("threshold" -> "0.9", "corpusId" -> "1"),
        )
        .debug
        .send(backend)

      _ = assertSuccess(response)

      body <- decodeBody[NearSpaceClusterResponse](response)
    } yield {
      println(
        io.circe
          .Encoder[Vector[(Int, BigDecimal)]]
          .apply(body.clusters.map { it =>
            (it.documents.length, it.lowestSimilarity)
          })
          .spaces2SortKeys,
      )
    }
  }
}
