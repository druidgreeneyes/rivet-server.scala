package similarity_test.ops

import doobie.implicits.*
import similarity.config.*
import similarity.persistence.Backend

package object setup {
  val resourceDataPath = "src/test/resources/data"
  val targetDataPath = "target/data"
  val csvFilePath = s"$resourceDataPath/documents.grouped.csv"
  val sqliteDbPath = s"$targetDataPath/test.sqlite.db"
  val sqliteDDL =
    sql"create table documents (id integer primary key, metadata varchar not null, bytes varchar)".update.run

  given config: Config = Config(
    persistence = PersistenceConfig(
      backend = Backend.SQL,
      sql = Some(
        SQLConfig(url = s"jdbc:sqlite:$sqliteDbPath"),
      ),
    ),
    env = EnvConfig(
      prefix = "RIVET",
    ),
  )
}
