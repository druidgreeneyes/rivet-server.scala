package similarity_test.ops.setup

import cats.effect.IO
import cats.syntax.all.*
import doobie.*
import doobie.implicits.*
import fs2.io.file.*
import io.circe.parser
import mouse.all.*
import scala.concurrent.duration.*
import scala.concurrent.duration.Duration
import similarity.app.document.DocumentController
import similarity.inject.*
import similarity.logging.log
import similarity.model.Metadata
import similarity.persistence.sql.transactorResource
import similarity.persistence.DocumentRepository
import similarity.syntax.*
import similarity_test.api.*
import sttp.capabilities.fs2.Fs2Streams
import sttp.client3.*
import sttp.model.MediaType
import sttp.model.StatusCode

class OpsSetupSuite extends APISuite("document") {
  val controller = {
    ResourceSuiteLocalFixture(
      "documentController",
      injectResource[DocumentController],
    )
  }

  val database = {
    ResourceSuiteLocalFixture(
      "database",
      injectResource[Transactor[IO]],
    )
  }

  override def munitFixtures: Seq[Fixture[?]] = Seq(controller, database)

  override def munitTimeout: Duration = 1.minute

  override def controllerEndpoints = controller().Endpoints

  private def wipeAndRecreateTargetDatabase = {
    Files.forIO.deleteIfExists(Path(sqliteDbPath))
      *> Files.forIO.createDirectories(Path(targetDataPath))
      *> sqliteDDL
        .transact(database())
        .void
  }

  test("Construct a corpus database".ignore) {
    val csvFile = Files.forIO.readAll(Path(csvFilePath))
    val csvFileMetadata = {
      Metadata(
        "fileName" -> "test.csv",
        "documentColumn" -> "comment",
        "corpusIdColumn" -> "corpus",
      )
    }
    for {
      // given
      // delete database and re-create if it exists
      _ <- wipeAndRecreateTargetDatabase

      // when
      response <- basicRequest
        .put(
          uri"http://test.com/"
            .addPath("document")
            .addParams(getParams(csvFileMetadata)*),
        )
        .contentType(MediaType.TextCsv)
        .streamBody(Fs2Streams[IO])(csvFile)
        .debug
        .send(backend)

      // then
      _ = assertSuccess(response)

      body <- decodeBody[Seq[Metadata]](response)

      _ <- Files.forIO
        .readUtf8Lines(Path(csvFilePath))
        .compile
        .count
        .map { lineCount =>
          log.debug(s"Recieved ${body.size} documents")
          // Assume the test file has a header row
          // Assume an empty line at the end of the file
          assertEquals[Any, Any](body.size, lineCount - 2)
        }

      _ = body.foreach { md =>
        assertEquals(md.get("fileName"), csvFileMetadata.get("fileName"))
        assertEquals(md.contentType, Some(MediaType.TextCsv.toString))
        assert(md.riv.nonEmpty)
        assert(md.id.nonEmpty)

        // These two should come from the csv file
        assert(md.get("comment_id").nonEmpty)
        assert(md.get("hash").nonEmpty)
      }

    } yield ()
  }
}
