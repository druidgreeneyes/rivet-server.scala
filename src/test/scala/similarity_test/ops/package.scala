package similarity_test

import doobie.implicits.*
import similarity.config.*
import similarity.persistence.Backend
import similarity.syntax.option.*

package object ops {
  val targetDataPath = "target/data"
  val csvDbPath = s"$targetDataPath/test.sqlite.db"
  given Config = Config(
    persistence = PersistenceConfig(
      backend = Backend.SQL,
      sql = some(
        SQLConfig(
          url = s"jdbc:sqlite:$csvDbPath",
        ),
      ),
    ),
    env = EnvConfig(
      prefix = "RIVET",
    ),
  )
}
