package similarity_test.api

import cats.effect.IO
import cats.syntax.all.*
import io.circe.parser
import io.circe.Decoder
import munit.Location
import similarity.config.Config
import similarity.inject.*
import similarity.inject.given
import similarity.logging
import similarity.syntax.either.*
import similarity.util.endpoints.Endpoints
import sttp.capabilities.fs2.Fs2Streams
import sttp.client3.Response
import sttp.client3.StringBody
import sttp.client3.SttpBackend
import sttp.model.Encodings
import sttp.model.StatusCode
import sttp.model.Uri.PathSegment
import sttp.model.Uri.UriContext

abstract class APISuite(
    basePath: String = "",
)(using Config)
    extends munit.CatsEffectSuite,
      munit.ScalaCheckEffectSuite {

  def controllerEndpoints: Endpoints
  def baseUri = uri"http://test.com/$basePath"

  def backend: SttpBackend[cats.effect.IO, Fs2Streams[cats.effect.IO]] = {
    stubEndpoints(controllerEndpoints())
  }

  extension (obj: StringBody.type) {
    def apply(content: String) = StringBody(content, Encodings.Identity)
  }

  override def beforeAll(): Unit = {
    logging.configure
  }

  def assertSuccess(response: Response[_]) =
    assertEquals(response.code, StatusCode.Ok)

  inline def decodeBody[T: Decoder](
      response: Response[Either[String, String]],
  ): IO[T] = IO {
    response.body
      .leftMap { Throwable(_) }
      .flatMap(parser.decode[T]) match {
      case Left(value) => throw value
      case Right(body) => body
    }
  }

  inline def bodyAssertions[T: Decoder](
      response: Response[Either[String, String]],
  )(
      assertions: (T => Unit)*,
  ): IO[Unit] = for {
    body <- decodeBody[T](response)
    _ <- assertions.foldLeft(IO.unit) { (io, assertion) =>
      io.as(assertion(body))
    }
  } yield ()
}
