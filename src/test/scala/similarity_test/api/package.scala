package similarity_test

import cats.effect.IO
import similarity.logging.log
import similarity.model.*
import sttp.capabilities.fs2.Fs2Streams
import sttp.client3.*
import sttp.client3.httpclient.fs2.HttpClientFs2Backend
import sttp.client3.testing.*
import sttp.model.*
import sttp.monad.*
import sttp.monad.MonadAsyncError
import sttp.tapir.server.stub.TapirStubInterpreter
import sttp.tapir.server.ServerEndpoint

package object api {
  private[api] def stubClient: TapirStubInterpreter[IO, Fs2Streams[IO], Unit] =
    TapirStubInterpreter(sttpBackendStub)
  private[api] def sttpBackendStub: SttpBackendStub[IO, Fs2Streams[IO]] =
    HttpClientFs2Backend.stub

  def stubEndpoints(
      serverEndpoints: List[
        ServerEndpoint[Fs2Streams[IO], IO],
      ],
  ): SttpBackend[IO, Fs2Streams[IO]] = {
    stubClient.whenServerEndpointsRunLogic(serverEndpoints).backend()
  }

  def getParams(m: Metadata): Seq[(String, String)] =
    m.toSeq

  extension [R <: RequestT[_, _, _]](r: R) {
    def debug: R = {
      log.debug(r.show())
      r
    }
  }
}
