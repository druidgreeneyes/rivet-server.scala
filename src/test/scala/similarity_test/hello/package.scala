package similarity_test

import cats.effect.IO
import mouse.any.*
import scala.concurrent.ExecutionContext
import similarity.app.hello.*
import similarity.app.hello.given
import similarity.inject.*
import similarity.inject.given
import similarity.logging.log
import sttp.client3.SttpBackend

package object hello {}
