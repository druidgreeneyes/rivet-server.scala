package similarity_test.hello

import cats.syntax.show.*
import org.scalacheck.effect.PropF
import org.scalacheck.Gen
import similarity.app.hello.HelloController
import similarity.inject.inject
import similarity.logging.log
import similarity.syntax.*
import similarity_test.api.*
import similarity_test.config.given
import sttp.client3.*
import sttp.model.StatusCode

class HelloApiSuite extends APISuite(basePath = "hello") {
  override lazy val controllerEndpoints = inject[HelloController].Endpoints

  private val randomNames =
    Gen.listOfN(6, Gen.asciiPrintableChar).map { _.mkString }

  test("GET /hello should accept a 'name' parameter") {
    PropF.forAllF(randomNames) { (name: String) =>
      for {
        // when
        response <- basicRequest
          .get(baseUri.addParam("name", name))
          .send(backend)

        // then
        _ = assertSuccess(response)
        _ = assertEquals(response.body, Right(s"Hello $name"))
      } yield ()

    }
  }
}
