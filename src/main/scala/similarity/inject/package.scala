package similarity

import cats.effect.kernel.Resource
import cats.effect.unsafe.IORuntime
import cats.effect.IO
import cats.syntax.traverse.*
import org.tpolecat.typename.TypeName
import scala.collection.mutable
import scala.quoted.*
import similarity.syntax.option.*

package object inject {
  trait Injector[T] extends (() => (_ <: T)) {}

  object Injector {
    def apply[T](t: => T) = () => t

    def deriveExpr[T: Type](t: Expr[T])(using Quotes): Expr[Injector[T]] = '{
      () =>
        $t
    }

    inline given derived[T](using t: T): Injector[T] = ${ deriveExpr[T]('t) }
    inline given derivedFromResource[T](using
        resource: Resource[IO, T],
    ): Injector[Resource[IO, T]] = () => resource
  }

  def inject[T](using i: Injector[T]) = i()
  def injectResource[T](using i: Injector[Resource[IO, T]]) = i()
}
