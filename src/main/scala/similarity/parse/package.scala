package similarity

import cats.effect.IO
import com.github.druidgreeneyes.rivet.core.labels.ArrayRIV
import fs2.Stream
import java.io.InputStream
import similarity.model.*
import sttp.model.MediaType

package object parse {
  type DocumentStream = IOStream[DocumentEntity]
  type ByteStream = IOStream[Byte]

  sealed trait MediaParser {
    def parse(
        input: ByteStream,
        metadata: Metadata,
    ): DocumentStream
  }

  object MediaParser {
    def apply(mediaType: MediaType) = mediaType match {
      case MediaType.TextPlain              => BytesParser
      case MediaType.TextPlainUtf8          => BytesParser
      case MediaType.TextCsv                => CSVParser
      case MediaType.ApplicationOctetStream => BytesParser
      case _ =>
        throw IllegalArgumentException(
          s"No parser available for media type ${mediaType.toString}",
        )
    }

    object BytesParser extends MediaParser {
      override def parse(
          input: ByteStream,
          metadata: Metadata,
      ): DocumentStream = {
        input.chunkAll.map { bytes =>
          DocumentEntity(
            metadata = metadata,
            bytes = bytes.toArray,
          )
        }
      }
    }

    object CSVParser extends MediaParser {
      override def parse(
          input: ByteStream,
          metadata: Metadata,
      ): DocumentStream = csv.parse(input, metadata)
    }
  }
}
