package similarity.parse

import com.github.druidgreeneyes.rivet.core.labels.ArrayRIV
import fs2.data.csv.*
import fs2.data.text.utf8.*
import fs2.Stream
import mouse.any.*
import scala.util.Random
import similarity.exception.PublicException
import similarity.logging.log
import similarity.model.*
import similarity.syntax.option.*

object csv {
  class CSVParseException(message: String) extends PublicException(message) {}

  def parse(input: ByteStream, metadata: Metadata): DocumentStream = {
    val documentColumn: String = metadata.get("documentColumn").orThrow {
      CSVParseException(
        "In order to parse a csv file we need to know what column to read documents from. Please pass the 'documentColumn' query parameter with the named column where we should find your documents.",
      )
    }
    val corpusIdColumn: Option[String] = metadata.get("corpusIdColumn")
    log.debug(
      s"Parsing csv input, assuming document text lives under column '${documentColumn}'",
    )
    for {
      row <- input.through(decodeUsingHeaders[Map[String, String]]())
      doc <-
        row
          .get(documentColumn)
          .ifEmpty { _ =>
            log.warn(s"row contains no document: $row")
          }
          .map { text =>
            val md = corpusIdColumn match {
              case None => metadata ++ row - documentColumn
              case Some(col) =>
                row.get(col) match {
                  case None => metadata ++ row - documentColumn
                  case Some(value) =>
                    metadata ++ row - documentColumn - col + ("corpusId" -> value)
                }
            }

            DocumentEntity(
              metadata = md,
              text = text,
            )
          } |> Stream.fromOption.apply
    } yield doc
  }
}
