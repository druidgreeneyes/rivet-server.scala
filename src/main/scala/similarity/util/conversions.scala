package similarity.util

import com.github.druidgreeneyes.rivet.core.labels.ArrayRIV
import com.github.druidgreeneyes.rivet.core.labels.RIV
import similarity.syntax.either.*
import sttp.model.MediaType

object conversions {
  given (String => Int) = _.toInt
  given (String => MediaType) =
    MediaType.parse(_).fold(msg => throw IllegalStateException(msg), identity)
}
