package similarity.util

import similarity.exception.PublicException
import similarity.logging.log
import sttp.tapir.stringBody
import sttp.tapir.DecodeResult

object tapir {
  def throwableBody = {
    stringBody.mapDecode[Throwable](s => DecodeResult.Value(Throwable(s)))(
      t => {
        log.error(t)
        t match {
          case p: PublicException => p.getMessage()
          case t                  => ""
        }
      },
    )
  }
}
