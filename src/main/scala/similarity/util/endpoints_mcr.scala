package similarity.util

import scala.quoted.*
import similarity.util.endpoints.PathString
import sttp.model.Uri.PathSegment

object endpoints_mcr {

  private val pathRegex = "^[^/]+$".r

  private def derivePathStringImpl[S <: String: Type](using
      Quotes,
  ): Expr[PathString[S]] = {
    Type.valueOfConstant[S] match {
      case None =>
        throw IllegalArgumentException(
          s"Type ${Type.show[S]} is not a singleton constant!",
        )
      case Some(value) =>
        value match {
          case v if pathRegex.matches(v) =>
            '{
              new PathString[S] {
                def segment = PathSegment(${ Expr(v) })
              }
            }
          case v =>
            throw IllegalArgumentException(
              s"Value $v is not a suitable endpoint path segment!",
            )
        }

    }
  }

  inline def derivePathString[S <: String]: PathString[S] = ${
    derivePathStringImpl[S]
  }
}
