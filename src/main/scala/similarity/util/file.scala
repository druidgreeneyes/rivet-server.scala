package similarity.util

import cats.syntax.either.*
import java.io.FileNotFoundException
import java.nio.file.*
import mouse.any.*
import scala.compiletime.ops.any.ToString
import scala.quoted.*

object file {
  given ToExpr[Path] with {
    override def apply(x: Path)(using Quotes): Expr[Path] = {
      '{ Path.of(${ Expr(x.toString()) }) }
    }
  }

  def resolveExtensions[E <: Tuple: Type](using Quotes): List[String] = {
    Type.of[E] match {
      case '[head *: tail] =>
        Type.valueOfConstant[head].get.toString :: resolveExtensions[tail]
      case _ => Nil
    }
  }

  def resolveImpl[Extensions <: Tuple: Type](pathExpr: Expr[String])(using
      Quotes,
  ): Expr[Path] = {
    import quotes.reflect.*
    val path = pathExpr.valueOrAbort
    val extensions = resolveExtensions[Extensions]
    val res = extensions
      .foldLeft[Either[FileNotFoundException, Path]](
        Left(FileNotFoundException(s"$path(${extensions.mkString(" | ")})")),
      ) { (eith, ext) =>
        eith match {
          case r: Right[_, _] => r
          case left => {
            Path.of(path + ext) match {
              case it if Files.exists(it) => Right(it)
              case it =>
                println(s"No file at: ${it}")
                left
            }
          }
        }
      }
      .valueOr { throw _ }
    Expr(res)
  }

  sealed trait FileType {
    type Extensions <: Tuple
  }

  object FileType {
    object YAML extends FileType {
      type Extensions = (".yml", ".yaml")
    }
  }

  extension [T <: FileType](t: T) {
    inline def resolve(path: String) = ${
      resolveImpl[t.Extensions]('path)
    }
  }
}
