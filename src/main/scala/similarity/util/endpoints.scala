package similarity.util

import cats.effect.IO
import org.http4s.HttpRoutes
import org.tpolecat.typename.TypeName
import scala.compiletime.constValue
import similarity.logging.log
import similarity.model.*
import sttp.capabilities.fs2.Fs2Streams
import sttp.capabilities.Streams
import sttp.model.Uri.PathSegment
import sttp.model.Uri.Segment
import sttp.tapir.*
import sttp.tapir.server.ServerEndpoint

object endpoints {
  type Endpoint = ServerEndpoint[Fs2Streams[IO], IO]

  trait Endpoints {
    def apply(): List[ServerEndpoint[Fs2Streams[IO], IO]]
  }

  trait PathString[S] {
    def segment: Segment
  }

  object PathString {
    inline given derived[S <: String]: PathString[S] =
      endpoints_mcr.derivePathString[S]
  }

  trait Serve[S <: String] {
    def apply(): sttp.tapir.Endpoint[Unit, Unit, Unit, Unit, Any]
  }

  object Serve {
    given derived[S <: String](using ps: PathString[S]): Serve[S] = {
      new Serve[S] {
        override def apply() = endpoint.in(ps.segment.encoded)
      }
    }
  }

  def serve[S <: String](using s: Serve[S]) = s()

  trait Controller {
    def Endpoints: Endpoints
  }
}
