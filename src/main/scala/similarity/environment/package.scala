package similarity

import org.tpolecat.typename.TypeName
import scala.compiletime.constValue
import similarity.config.*
import similarity.inject.*
import similarity.inject.given
import similarity.logging.log
import similarity.syntax.option.*

package object environment {
  class EnvironmentVariableNotPresent(label: String)
      extends Throwable(
        s"Environment variable '${label}' has no value or is not set!",
      )
  object EnvironmentVariableNotPresent {
    def apply(label: String): EnvironmentVariableNotPresent =
      new EnvironmentVariableNotPresent(label)
    def apply[T <: String](using
        tn: TypeName[T],
    ): EnvironmentVariableNotPresent = EnvironmentVariableNotPresent(tn.value)
  }

  trait Env[Label <: String] {
    def name: String
    private def fail = throw EnvironmentVariableNotPresent(name)
    def valueOpt: Option[String] = {
      log.debug(s"reading environment variable: $name")
      sys.env.get(name)
    }
    def value: String = valueOpt.getOrElse { fail }
    def valueOpt[T](using conv: String => T): Option[T] = {
      valueOpt.map(conv)
    }
    def value[T](using String => T): T = {
      valueOpt[T].getOrElse { fail }
    }
  }

  object Env {
    inline def apply[Label <: String](pref: String): Env[Label] = {
      new Env[Label] {
        override def name: String = pref + "_" + constValue[Label]
      }
    }

    inline def raw[Label <: String](v: String)(using Config): Env[Label] = {
      new Env[Label] {
        override def name: String = config.env.prefix + "_" + constValue[Label]
        override def valueOpt = some(v)

      }
    }
  }

  inline given env[Label <: String](using Config): Env[Label] = {
    Env[Label](config.env.prefix)
  }
}
