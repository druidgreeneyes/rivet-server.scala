package similarity.syntax

import com.github.druidgreeneyes.rivet.core.labels.RIV
import java.math.MathContext

object riv {
  val mc = MathContext(2)

  extension (riv: RIV) {
    def |%|(other: RIV): BigDecimal =
      BigDecimal(riv.similarityTo(other), mc)
  }

  def similarityZero = BigDecimal(0, mc)
}
