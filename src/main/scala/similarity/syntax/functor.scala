package similarity.syntax

import cats.syntax.functor.*
import cats.Bifunctor
import cats.Functor

object functor {
  def map[F[_], T, R](f: T => R)(functor: F[T])(using
      Functor[F],
  ): F[R] =
    functor.map(f)

  def ftap[F[_], T](
      f: T => Unit,
  )(functor: F[T])(using Functor[F]): F[T] = {
    functor.map { t =>
      f(t)
      t
    }
  }
}
