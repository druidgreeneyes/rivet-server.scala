package similarity.syntax

import cats.syntax.flatMap.*
import cats.FlatMap

object flatmap {
  def flatMap[F[_], T, R](f: T => F[R])(m: F[T])(using FlatMap[F]) = m >>= f
}
