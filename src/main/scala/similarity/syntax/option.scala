package similarity.syntax

object option {
  extension [T](opt: Option[T]) {
    def orThrow(e: => Throwable): T = opt.getOrElse { throw e }
    def ifEmpty(effect: opt.type => Unit): opt.type = {
      if (opt.isEmpty) {
        effect(opt)
      }
      opt
    }
  }
  def some[T](t: T): Option[T] = Some(t)
}
