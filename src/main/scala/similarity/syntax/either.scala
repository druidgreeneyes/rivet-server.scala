package similarity.syntax

object either {
  extension [T](e: Either[Throwable, T]) {
    def orThrow: T = e.fold(
      { throw _ },
      identity,
    )
  }
}
