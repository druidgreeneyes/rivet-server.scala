package similarity

package object exception {
  class PublicException(message: String) extends Throwable(message) {
    def this(message: String, cause: Throwable) = {
      this(message)
      initCause(cause)
    }

    def this(cause: Throwable) = {
      this(Option(cause).map(_.toString).orNull, cause)
    }
  }
}
