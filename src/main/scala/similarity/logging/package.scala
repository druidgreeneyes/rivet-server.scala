package similarity

import mouse.any.*
import scribe.*
import similarity.config.Config
import similarity.environment.*
import similarity.environment.given
import similarity.inject.*
import similarity.inject.given

package object logging {
  type LogLevel = Env["LOG_LEVEL"]

  private def resolveLevel(opt: Option[String]) = opt.map(Level.apply)

  def configure(using Config): Unit = {
    val level = inject[LogLevel]
    Logger.root
      .clearHandlers()
      .clearModifiers()
      .withHandler(minimumLevel = {
        level.valueOpt
          .flatMap(Level.get)
          .orElse(Some(Level.Info))
          .unsafeTap(o => println(s"Log Level: $o"))
      })
      .replace()
  }

  object log {
    export scribe.debug
    export scribe.error
    export scribe.info
    export scribe.log
    export scribe.trace
    export scribe.warn
  }
}
