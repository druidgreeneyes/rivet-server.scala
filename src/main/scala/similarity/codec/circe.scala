package similarity.codec

import com.github.druidgreeneyes.rivet.core.labels.ArrayRIV
import com.github.druidgreeneyes.rivet.core.labels.RIV
import io.circe.generic.semiauto.deriveCodec
import io.circe.Codec
import io.circe.Decoder
import io.circe.Encoder
import similarity.logging.log
import similarity.model.*

given rivEncoder: Encoder[RIV] = Encoder.encodeString.contramap(_.toString)
given rivDecoder: Decoder[RIV] = Decoder.decodeString.map { s =>
  ArrayRIV.fromString(s)
}
given docCodec: Codec[DocumentEntity] = deriveCodec
