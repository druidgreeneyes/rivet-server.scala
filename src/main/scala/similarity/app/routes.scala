package similarity.app

import cats.effect.kernel.Resource
import cats.effect.unsafe.IORuntime
import cats.effect.IO
import cats.syntax.show.*
import cats.Show
import mouse.any.*
import org.http4s.blaze.server.BlazeServerBuilder
import org.http4s.server.Router
import org.http4s.server.Server
import org.http4s.HttpRoutes
import scala.collection.mutable
import scala.concurrent.ExecutionContext
import similarity.app.document.*
import similarity.app.document.given
import similarity.app.document.DocumentController
import similarity.app.hello.*
import similarity.app.hello.given
import similarity.app.ops.OpsController
import similarity.config.Config
import similarity.environment.given
import similarity.environment.Env
import similarity.inject.*
import similarity.inject.given
import similarity.logging.log
import similarity.syntax.flatmap.*
import similarity.syntax.functor.*
import similarity.util.conversions.given
import sttp.tapir.server.http4s.Http4sServerInterpreter

/** This is where we compose all the routes from various Controller objects, and
  * provide a server that we can invoke from main or for testing.
  *
  * See [[similarity.riv.RIVController]] and
  * [[similarity.hello.HelloController]]
  */
object routes {
  type HttpPort = Env["HTTP_PORT"]

  private def routes(using
      Config,
      IORuntime,
  ): Resource[IO, HttpRoutes[IO]] = {
    for {
      opsController <- injectResource[OpsController]
      documentController <- injectResource[DocumentController]
      helloController = inject[HelloController]
    } yield {
      List(
        opsController.Endpoints,
        documentController.Endpoints,
        helloController.Endpoints,
      ) |> flatMap { _() }
        |> ftap { e => log.debug(e.show) }
        |> Http4sServerInterpreter[IO]().toRoutes
    }
  }

  /** Build the server, wrapped in [[cats.effect.IO]].
    *
    * Note that I've chosen to avoid a Monads-all-the-way-down sort of approach,
    * here and where we directly reference the underlying server configuration
    * should be the only places we actually see high-level monads. See
    * [[similarity.routes]]
    *
    * @param args
    *   Ignored
    * @return
    */
  def server(
      ec: ExecutionContext,
  )(using Config, IORuntime): Resource[IO, Server] = {
    for {
      routes <- routes
      port = inject[HttpPort]
      server <- BlazeServerBuilder[IO]
        .withExecutionContext(ec)
        .bindHttp(port.valueOpt[Int].getOrElse(8096), "localhost")
        .withHttpApp(Router("/" -> routes).orNotFound)
        .resource
    } yield server
  }
}
