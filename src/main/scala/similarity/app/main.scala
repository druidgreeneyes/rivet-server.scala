package similarity.app

import cats.effect.kernel.Resource
import cats.effect.unsafe.IORuntime
import cats.effect.ExitCode
import cats.effect.IO
import cats.effect.IOApp
import cats.effect.ResourceApp
import cats.syntax.all.*
import org.http4s.server.Server
import scala.concurrent.ExecutionContext
import similarity.app.routes.server
import similarity.config.Config
import similarity.inject.*
import similarity.inject.given
import similarity.logging

object main extends ResourceApp.Forever {

  given Config = Config.load
  given ioRuntime: IORuntime = IORuntime.global

  /** Execution context used to provide asynchrony to the server below
    */
  val ec: ExecutionContext = ioRuntime.compute
  val serverResource: Resource[IO, Server] = server(ec)

  /** Run the server, wrapped in [[cats.effect.Resource]].
    *
    * @param args
    *   Ignored
    * @return
    */
  override def run(args: List[String]): Resource[IO, Unit] = {
    logging.configure
    for {
      _ <- serverResource
    } yield ()
  }
}
