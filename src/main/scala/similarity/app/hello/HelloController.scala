package similarity.app.hello

import cats.effect.IO
import similarity.config.Config
import similarity.inject.*
import similarity.inject.given
import similarity.logging.log
import similarity.util.endpoints.*
import similarity.util.endpoints.given
import similarity.util.tapir.*
import sttp.tapir.*

class HelloController(using Config, Serve["hello"]) {
  private lazy val helloService = HelloService
  def hello(name: String): IO[String] = IO {
    helloService.hello(name)
  }

  object Endpoints extends Endpoints {
    val getHello = serve.get
      .in(query[String]("name"))
      .out(stringBody)
      .errorOut(throwableBody)
      .serverLogicRecoverErrors(hello)

    def apply() = List(getHello)
  }
}

object HelloController {
  given impl(using Config): HelloController = HelloController()
}
