package similarity.app.hello

import similarity.logging.log

object HelloService {
  def hello(name: String) = {
    s"Hello $name"
  }
}
