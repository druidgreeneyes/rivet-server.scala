package similarity.app.document

class TextPreProcessingService private () {
  def parseTokens(bytes: Array[Byte]): Array[String] = {
    String(bytes).split(" ")
  }
}

object TextPreProcessingService {
  given impl: TextPreProcessingService = new TextPreProcessingService()
}
