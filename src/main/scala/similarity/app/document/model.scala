package similarity.app.document

import similarity.model.*

object model {
  case class DocumentSubmission(
      api_key: String,
      metadata: Metadata,
      file: IOStream[Byte],
  )
}
