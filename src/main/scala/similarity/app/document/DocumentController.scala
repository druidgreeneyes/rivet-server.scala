package similarity.app.document

import cats.effect.kernel.Resource
import cats.effect.unsafe.IORuntime
import cats.effect.IO
import cats.syntax.either.*
import fs2.Stream
import io.circe.generic.auto.*
import scala.collection.mutable.ListBuffer
import similarity.app.document.model.*
import similarity.codec.given
import similarity.config.Config
import similarity.inject.*
import similarity.inject.given
import similarity.logging.log
import similarity.model.*
import similarity.model.given
import similarity.syntax.*
import similarity.util.endpoints.*
import similarity.util.endpoints.given
import similarity.util.tapir.*
import sttp.capabilities.fs2.Fs2Streams
import sttp.model.HeaderNames
import sttp.model.MediaType
import sttp.model.QueryParams
import sttp.model.StatusCode
import sttp.tapir.*
import sttp.tapir.generic.auto.*
import sttp.tapir.json.circe.jsonBody
import sttp.tapir.CodecFormat.MultipartFormData
import sttp.tapir.EndpointIO.FixedHeader

case class DocumentDoesNotExist(val fileName: String)
    extends Throwable(
      s"Document with file name ${fileName} does not exist! Try submitting it first.",
    )

class DocumentController private (private val documentService: DocumentService)(
    using Serve["document"],
) extends Controller {

  def submit(
      contentType: String,
      api_key: String,
      codex: String,
      params: QueryParams,
      input: IOStream[Byte],
  ): IO[Seq[Metadata]] = {
    val metadata = Metadata(
      params.toMap
        + ("contentType" -> contentType.toString),
    )
    log.debug(s"File Codex: ${codex}")
    val submission = DocumentSubmission(api_key, metadata, input)
    documentService.save(submission).compile.toList.map { res =>
      log.debug(s"Saved ${res.size} documents!")
      res
    }
  }

  def show(id: String, api_key: String): IO[IOStream[Byte]] = {
    IO {
      documentService.getDocumentContents(id)
    }
  }

  def delete(id: String, api_key: String) = documentService.deleteDocument(id)

  def metadata(id: String, api_key: String) =
    documentService.getDocumentMetadata(id)

  def getAllDocuments(api_key: String) = {
    documentService
      .getAllDocuments()
      .map((md) => {
        md - "riv" - "contentType"
      })
      .compile
      .toList
  }

  object Endpoints extends Endpoints {
    val putDocument = serve.put
      .in(header[String](HeaderNames.ContentType))
      .in(header[String]("X-API-Key"))
      .in(header[String]("X-CodeX"))
      .in(queryParams)
      .in(streamBinaryBody(Fs2Streams[IO])(CodecFormat.OctetStream()))
      .out(jsonBody[Seq[Metadata]])
      .errorOut(throwableBody)
      .serverLogicRecoverErrors(submit)

    val getDocument = serve.get
      .in(query[String]("id"))
      .in(header[String]("X-API-Key"))
      .out(streamBinaryBody(Fs2Streams[IO])(CodecFormat.OctetStream()))
      .errorOut(throwableBody)
      .serverLogicRecoverErrors(show)

    val getMetadata = serve.get
      .in("metadata")
      .in(query[String]("id"))
      .in(header[String]("X-API-Key"))
      .out(jsonBody[Metadata])
      .errorOut(throwableBody)
      .serverLogicRecoverErrors(metadata)

    val deleteDocument = serve.delete
      .in(query[String]("id"))
      .in(header[String]("X-API-Key"))
      .out(jsonBody[Metadata])
      .errorOut(throwableBody)
      .serverLogicRecoverErrors(delete)

    val listAllDocuments = serve.get
      .in("all")
      .in(header[String]("X-API-Key"))
      .out(jsonBody[List[Metadata]])
      .errorOut(throwableBody)
      .serverLogicRecoverErrors(getAllDocuments)

    override def apply() = List(
      putDocument,
      getDocument,
      listAllDocuments,
      getMetadata,
      deleteDocument,
      getMetadata,
    )
  }
}

object DocumentController {
  given impl(using Config, IORuntime): Resource[IO, DocumentController] =
    injectResource[DocumentService].map(new DocumentController(_))
}
