package similarity.app.document

import cats.effect.kernel.Resource
import cats.effect.std.AtomicCell
import cats.effect.std.Random
import cats.effect.unsafe.IORuntime
import cats.effect.IO
import com.github.druidgreeneyes.rivet.core.labels.ArrayRIV
import com.github.druidgreeneyes.rivet.core.labels.RIV
import com.github.druidgreeneyes.rivet.core.labels.RIVs
import fs2.Stream
import io.brunk.tokenizers.Tokenizer
import java.util.concurrent.atomic.AtomicInteger
import java.util.UUID
import similarity.app.document.model.*
import similarity.app.document.model.given
import similarity.config.Config
import similarity.inject.*
import similarity.inject.given
import similarity.logging.log
import similarity.model.*
import similarity.parse.*
import similarity.persistence.*
import similarity.persistence.given
import similarity.syntax.*
import similarity.syntax.riv.*
import similarity.util.conversions.given
import sttp.model.MediaType
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter

class DocumentService private (
    private val docRepository: DocumentRepository,
    val textPreProcessingService: TextPreProcessingService,
)(using
    Config,
) {
  private val rivSize = 8000
  private val rivNNZ = 4

  private val rand = Random.scalaUtilRandom[IO]

  private val rivGenerator = RIVs.generator(
    rivSize,
    rivNNZ,
    { (keys, vals, size) =>
      new ArrayRIV(keys, vals, size)
    },
  )

  def save(submission: DocumentSubmission): IOStream[Metadata] = {
    val byteStream = submission.file
    val mediaType = {
      submission.metadata.contentType.flatMap {
        MediaType.parse(_).toOption
      } match {
        case None =>
          throw IllegalArgumentException(
            "Uploaded file must have a specified content type!",
          )
        case Some(value) => value
      }
    }

    val metadata = submission.metadata

    val parser = MediaParser(mediaType)
    log.debug(s"inbound metadata ${metadata.toString}")
    val count = AtomicInteger(0)
    parser
      .parse(byteStream, metadata)
      .map { doc =>
        val tokens = textPreProcessingService.parseTokens(doc.bytes)
        val riv = {
          tokens.map { rivGenerator(_) }.foldLeft(ArrayRIV.empty(rivSize)) {
            (a, b) => a.destructiveAdd(b)
          }
        }
        count.incrementAndGet()
        doc.updateMetadata("riv" -> riv.toString, 
          "size" -> String(doc.bytes).length.toString,
          "when" -> LocalDateTime.now(ZoneOffset.UTC).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")))
      }
      .through(docRepository.saveDocuments)
      .onFinalize(IO {
        log.debug(s"Processed ${count.get()} documents!")
      })
  }

  def getAllDocuments() = docRepository.getAllMetadata()

  def deleteDocument(id: String) = docRepository.deleteDocument(id)
  
  def getDocumentContents(id: String): IOStream[Byte] = {
    docRepository.getContentStream(id)
  }

  def getDocumentMetadata(id: String): IO[Metadata] = {
    log.debug(s"getting metadata for ${id}")
    docRepository.getMetadata(id)
  }
}

object DocumentService {
  given impl(using Config, IORuntime): Resource[IO, DocumentService] = {
    for {
      docRepository <- injectResource[DocumentRepository]
      textPreProcessingService = inject[TextPreProcessingService]
    } yield {
      new DocumentService(
        docRepository,
        textPreProcessingService,
      )
    }
  }
}
