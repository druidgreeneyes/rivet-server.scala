package similarity.app

import cats.effect.IO
import com.github.druidgreeneyes.rivet.core.labels.RIV
import fs2.Stream
import mouse.any.*
import scala.annotation.tailrec
import scala.collection.parallel.immutable.ParVector
import scala.collection.parallel.CollectionConverters.*
import scala.reflect.ClassTag
import similarity.logging.log
import similarity.model.*
import similarity.syntax.riv.*

package object ops {

  @tailrec
  private def squareMapRec[T, R](
      things: Vector[T],
      f: (T, T) => R,
      i: Int,
      j: Int,
      out: Array[R],
      c: Int,
  ): Vector[R] = {
    if (c == out.length) {
      out.toVector
    } else if (j == things.length) {
      if (i % math.max(things.length / 10, 1) == 0) {
        log.trace(s"Row $i complete!")
      }
      squareMapRec[T, R](things, f, i + 1, i + 2, out, c)
    } else {
      out(c) = f(things(i), things(j))
      squareMapRec[T, R](things, f, i, j + 1, out, c + 1)
    }
  }

  extension [T](things: Vector[T]) {
    def squareMap[R](f: (T, T) => R)(using ClassTag[R]): Vector[R] = {
      val expectedResults = {
        things.length match {
          case it if it < 2 => {
            log.error("Can't run a square computation with < 2 elements!")
            0
          }
          case it if it == 2 => 1
          case it => {
            math.ceil(it * it / 2.0).intValue - it <| { l =>
              log.debug(
                s"Running a square computation against ${things.length} elements. Expect ~${l} results.",
              )
            }
          }
        }
      }
      val res = Array.fill[R](expectedResults)(null.asInstanceOf[R])
      squareMapRec(things, f, 0, 1, res, 0).filterNot(_ == null)
    }

    def squareStream: Stream[IO, (T, T)] = {
      Stream.unfoldLoop((0, 1)) { (i, j) =>
        val value = (things(i), things(j))
        val next = if (j + 1 >= things.length) {
          if (i + 1 >= things.length) {
            None
          } else {
            Some((i + 1, i + 2))
          }
        } else {
          Some((i, j + 1))
        }
        (value, next)
      }
    }
  }

  def compareAllRivs(
      metadata: Vector[Metadata],
  ): Vector[SingleComparison] = {
    metadata.squareMap { (a, b) =>
      SingleComparison(a.id.get, b.id.get, a.riv.get |%| b.riv.get)
    } <| { res =>
      log.debug(s"Square comparison complete! ${res.length} comparisons.")
    }
  }

  def computeLowestSimilarity(rivs: Vector[RIV]): BigDecimal = {
    if (rivs.length < 2) {
      0
    } else {
      rivs.squareMap { _ |%| _ }.min
    }
  }
}
