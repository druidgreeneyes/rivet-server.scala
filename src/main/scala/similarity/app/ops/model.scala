package similarity.app.ops

import cats.data.NonEmptySet
import cats.syntax.all.*
import com.github.druidgreeneyes.rivet.core.labels.ArrayRIV
import com.github.druidgreeneyes.rivet.core.labels.RIV
import com.github.druidgreeneyes.rivet.core.labels.RIVs
import similarity.model.*

case class SingleComparison(
    val docA: String,
    val docB: String,
    val value: BigDecimal,
)

type CosineSimilarityResponse = SingleComparison

case class NearSpaceClusterResponse(
    val threshold: BigDecimal,
    val clusters: Vector[NearSpaceCluster],
)

object NearSpaceClusterResponse {
  def apply(internal: NearSpaceClusters): NearSpaceClusterResponse =
    NearSpaceClusterResponse(internal.threshold, internal.groups.toVector)
}
