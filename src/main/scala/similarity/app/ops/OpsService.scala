package similarity.app.ops

import cats.data.NonEmptySet
import cats.effect.kernel.Resource
import cats.effect.unsafe.IORuntime
import cats.effect.IO
import cats.kernel.Order
import cats.syntax.all.*
import com.github.druidgreeneyes.rivet.core.labels.ArrayRIV
import fs2.concurrent.Topic
import fs2.Stream
import java.util.concurrent.atomic.AtomicInteger
import scala.collection.parallel.CollectionConverters.*
import similarity.config.Config
import similarity.inject.*
import similarity.inject.given
import similarity.logging.log
import similarity.model.*
import similarity.persistence.DocumentRepository
import similarity.syntax.*
import similarity.syntax.riv.*

class OpsService private (private val docRepository: DocumentRepository) {
  def cosineSimilarity(
      docA: String,
      docB: String,
  ): IO[CosineSimilarityResponse] = {
    for {
      a <- docRepository.getMetadata(docA)
      b <- docRepository.getMetadata(docB)
      aRiv <- IO { a.riv.get }
      bRiv <- IO { b.riv.get }
    } yield {
      SingleComparison(
        docA,
        docB,
        aRiv |%| bRiv,
      )
    }
  }

  def multiSims(
    ids: String
  ): IO[Vector[SingleComparison]] = {
    val IDS = ids.split(",")
    docRepository
          .getMetadata(IDS)
          .compile
          .toVector
          .map(compareAllRivs) 
    }

  def findDuplicates(
      threshold: BigDecimal,
      corpusId: String,
  ): IO[NearSpaceClusterResponse] = {
    val count = new AtomicInteger(0)
    docRepository
      .getCorpus(corpusId)
      .filter(md => md.riv.nonEmpty)
      .compile
      .toVector
      .flatMap { metadata =>
        NearSpaceClustering
          .detectAndAllocateRIVClusters(threshold, metadata)
          .map { it =>
            it.withGroups(
              it.groups.filterNot(g => g.lowestSimilarity < threshold),
            )
          }
      }
      .map { res => NearSpaceClusterResponse(res) }
  }
}

object OpsService {
  given impl(using Config, IORuntime): Resource[IO, OpsService] =
    injectResource[DocumentRepository].map { new OpsService(_) }
}
