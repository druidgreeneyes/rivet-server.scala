package similarity.app.ops

import cats.effect.std.AtomicCell
import cats.effect.IO
import cats.syntax.all.*
import cats.Foldable
import com.github.druidgreeneyes.rivet.core.labels.ArrayRIV
import com.github.druidgreeneyes.rivet.core.labels.RIV
import com.github.druidgreeneyes.rivet.core.labels.RIVs
import fs2.Stream
import java.util.concurrent.atomic.AtomicInteger
import java.util.concurrent.atomic.AtomicReference
import mouse.all.*
import scala.annotation.tailrec
import scala.collection.parallel.immutable.ParVector
import scala.collection.parallel.CollectionConverters.*
import scala.collection.ArrayOps
import similarity.logging.log
import similarity.model.*
import similarity.syntax.option.*
import similarity.syntax.riv.*

case class NearSpaceCluster(
    val documents: Vector[Metadata],
) {
  val rivs = {
    documents.map(_.riv.get)
  }
  lazy val lowestSimilarity: BigDecimal = computeLowestSimilarity(rivs)
}

object NearSpaceCluster {
  def apply(metadata: Metadata*): NearSpaceCluster = NearSpaceCluster(
    metadata.toVector,
  )
}

case class NearSpaceClusters(
    val threshold: BigDecimal,
    val groups: Vector[NearSpaceCluster],
) {
  def withGroups(groups: Vector[NearSpaceCluster]): NearSpaceClusters =
    copy(groups = groups)

  def withGroupsWeak(groups: Iterable[NearSpaceCluster]): NearSpaceClusters = {
    withGroups(
      groups.toVector,
    )
  }

  lazy val lowestGroupSimilarity = groups.map { _.lowestSimilarity }.min
}

object NearSpaceClustering {
  def findLowestSimilarityAgainstCluster(
      riv: RIV,
      cluster: NearSpaceCluster,
  ): BigDecimal = {
    cluster.rivs.map { _ |%| riv }.min
  }

  def findMostSimilarCluster(clusters: NearSpaceClusters)(
      doc: Metadata,
  ): (Metadata, Int) = {
    val riv = doc.riv.get
    val (foundSim, foundIndex) = {
      clusters.groups.par.zipWithIndex
        .map { (cluster, idx) =>
          findLowestSimilarityAgainstCluster(riv, cluster) -> idx
        }
        .maxBy { (sim, _) => sim }
    }
    doc -> foundIndex
  }

  def redistributeClusters(
      clusters: NearSpaceClusters,
  ): IO[NearSpaceClusters] = IO {
    clusters.groups
      .flatMap { _.documents }
      .distinctBy { _.id }
      .par
      .map(findMostSimilarCluster(clusters))
      .groupBy { (_, idx) => idx }
      .values
      .map { v => NearSpaceCluster(v.unzip._1.toVector) }
      .toVector
      |> clusters.withGroups
  }

  def doRedistributionLoop(
      clusters: NearSpaceClusters,
      threshold: BigDecimal,
      loopMax: Int,
      currentLoop: Int = 0,
  ): IO[NearSpaceClusters] = {
    val loopResultReference = AtomicReference[NearSpaceClusters](clusters)
    Stream
      .iterateEval((0, clusters)) { (currentLoop, cls) =>
        val nextLoop = currentLoop + 1
        IO {
          log.info(
            s"Clusters: ${cls.groups.size}. Performing cluster redistribution $nextLoop...",
          )
        }.flatMap { _ =>
          redistributeClusters(cls)
        }.map { (nextLoop, _) }
      }
      .limit(loopMax)
      .takeThrough { (currentLoop, cls) =>
        val prev = if (currentLoop % 5 == 0) {
          log.info(
            s"Loop $currentLoop: Cluster lowest group cohesion: ${cls.lowestGroupSimilarity}",
          )
          loopResultReference.getAndSet(cls)
        } else {
          loopResultReference.get()
        }

        if (
          currentLoop > 0
          && currentLoop % 5 == 0
          && prev.groups.length == cls.groups.length
          && prev.lowestGroupSimilarity == cls.lowestGroupSimilarity
        ) {
          log.warn(
            s"Loop $currentLoop: Distributions don't appear to have meaningfully changed during this redistribution cycle!",
          )
          false
        } else {
          if (cls.lowestGroupSimilarity <= threshold) {
            true
          } else {
            log.info(
              s"Loop $currentLoop: Cluster similarity has topped the given threshold!",
            )
            false
          }
        }
      }
      .compile
      .lastOrError
      .map { (loop, cls) =>
        log.info(
          s"Cluster redistribution complete! Took ${loop} iterations.",
        )
        cls
      }
  }

  def detectAndAllocateRIVClusters(
      threshold: BigDecimal,
      metadata: Vector[Metadata],
  ): IO[NearSpaceClusters] = {
    log.debug("Populating initial cluster distribution...")
    val groups = metadata.map { NearSpaceCluster(_) }
    log.debug(s"Distributed ${groups.length} groups.")
    val clusters = NearSpaceClusters(threshold, groups)
    log.debug(s"Redistributing ${groups.length} documents...")
    doRedistributionLoop(clusters, threshold, 100, 0)
  }
}
