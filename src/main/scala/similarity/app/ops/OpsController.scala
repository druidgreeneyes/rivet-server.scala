package similarity.app.ops

import cats.effect.kernel.Resource
import cats.effect.unsafe.IORuntime
import cats.effect.IO
import io.circe.generic.auto.*
import similarity.config.Config
import similarity.inject.*
import similarity.inject.given
import similarity.logging.log
import similarity.model.*
import similarity.syntax.*
import similarity.util.endpoints.*
import similarity.util.tapir.*
import sttp.tapir.*
import sttp.tapir.generic.auto.*
import sttp.tapir.json.circe.jsonBody
import sttp.tapir.RawBodyType.StringBody

class OpsController private (opsService: OpsService)(using Serve["ops"]) {

  def cosineSimilarity(
      docA: String,
      docB: String,
      api_key: String
  ): IO[SingleComparison] = {
    opsService.cosineSimilarity(docA, docB)
  }

  def multiSims (
      api_key: String,
      ids: String
  ): IO[Vector[SingleComparison]] = {
    opsService.multiSims(ids)
  }

  def findDuplicates(
      threshold: Double,
      corpusId: String,
      api_key: String
  ): IO[NearSpaceClusterResponse] = {
    opsService.findDuplicates(threshold, corpusId)
  }

  object Endpoints extends Endpoints {

    val cosine = serve.post
      .in("cosine")
      .in(query[String]("docA").and(query[String]("docB")))
      .in(header[String]("X-API-Key"))
      .out(jsonBody[CosineSimilarityResponse])
      .errorOut(throwableBody)
      .serverLogicRecoverErrors(cosineSimilarity)

    val similarities = serve.post
      .in("similarity")
      .in(header[String]("X-API-Key"))
      .in(stringBody)
      .out(jsonBody[Vector[SingleComparison]])
      .errorOut(throwableBody)
      .serverLogicRecoverErrors(multiSims)

    val duplicates = serve.post
      .in("duplicates")
      .in(query[Double]("threshold").and(query[String]("corpusId")))
      .in(header[String]("X-API-Key"))
      .out(jsonBody[NearSpaceClusterResponse])
      .errorOut(throwableBody)
      .serverLogicRecoverErrors(findDuplicates)

    def apply() = {
      List(
        cosine,
        similarities,
        duplicates,
      ).tapEach { it => log.debug(it.show) }
    }
  }

}

object OpsController {
  given impl(using Config, IORuntime): Resource[IO, OpsController] =
    injectResource[OpsService].map { new OpsController(_) }
}
