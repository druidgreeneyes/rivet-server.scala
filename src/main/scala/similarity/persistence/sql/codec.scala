package similarity.persistence.sql

import doobie.enumerated.JdbcType
import doobie.util.Get
import doobie.util.Put
import doobie.util.Read
import io.circe.parser
import io.circe.Decoder
import java.sql.ResultSet
import mouse.any.*
import similarity.model.Metadata
import similarity.syntax.either.*

def resultSetToMetadata(rs: ResultSet, idx: Int): Metadata = {
  val res = for {
    json <- rs.getString(idx) |> parser.parse
    metadata <- Decoder[Metadata].decodeJson(json)
  } yield metadata
  res.orThrow
}

given getMetadata: Get[Metadata] = Get.Basic.one[Metadata](
  jdbcSources = JdbcType.VarChar,
  jdbcSourceSecondary = List(),
  get = resultSetToMetadata,
)

given putMetadata: Put[Metadata] = Put.Basic.one[Metadata](
  jdbcTarget = JdbcType.VarChar,
  put = (stmt, i, metadata) => stmt.setString(i, metadata.jsonStr),
  update = (rs, i, metadata) => {
    val old = resultSetToMetadata(rs, i)
    val merged = old ++ metadata
    rs.updateString(i, merged.jsonStr)
  },
)
