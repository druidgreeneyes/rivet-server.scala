package similarity.persistence.sql

import cats.data.NonEmptySeq
import cats.effect.implicits.*
import cats.effect.kernel.Resource
import cats.effect.kernel.Resource.ExitCase.Canceled
import cats.effect.kernel.Resource.ExitCase.Errored
import cats.effect.kernel.Resource.ExitCase.Succeeded
import cats.effect.std.Random
import cats.effect.unsafe.IORuntime
import cats.effect.IO
import cats.syntax.traverse.*
import doobie.*
import doobie.implicits.*
import doobie.util.fragment.Elem.Arg
import doobie.util.fragment.Elem.Opt
import doobie.util.log.LogHandler
import doobie.util.transactor.Transactor
import doobie.util.Read
import fs2.Chunk
import fs2.Stream
import java.util.UUID
import mouse.any.*
import similarity.config.Config
import similarity.inject.*
import similarity.inject.given
import similarity.logging.log
import similarity.model.*
import similarity.persistence.DocumentRepository
import similarity.syntax.option.*

given LogHandler = LogHandler(e => log.debug(e.toString))

class SQLRepository private (private val database: Transactor[IO])
    extends DocumentRepository {

  private val random = Random.scalaUtilRandom[IO]

  override def getDocument(id: ID): IO[DocumentEntity] = {
    Fragment(
      "select * from documents where id = ? limit 1",
      List(Arg(id, summon)),
    )
      .query[(ID, DocumentEntity)]
      .unique
      .map { (id, doc) => doc.updateMetadata("id" -> id) }
      .transact(database)
  }

  private def prepareSave(thing: DocumentEntity): ConnectionIO[Metadata] = {
    sql"insert into documents (id, metadata, bytes) values (${thing.id}, ${thing.metadata}, ${thing.bytes})".update
      .withUniqueGeneratedKeys[Long]("id")
      .map { id => thing.metadata + ("id" -> id.toString) }
  }

  override def saveDocument(thing: DocumentEntity): IO[Metadata] = {
    prepareSave(thing)
      .transact(database)
      .flatMap { m => IO { log.debug(m.jsonStr); m } }
  }

  override def saveDocuments(
      things: IOStream[DocumentEntity],
  ): IOStream[Metadata] = {
    log.debug("Inserting documents...")
    things
      .map(prepareSave)
      .chunkN(10_000)
      .flatMap { chunk =>
        log.debug(s"Inserting chunk: ${chunk.size}")
        Stream
          .evalUnChunk(chunk.sequence)
          .transact(database)
      }
      .onFinalizeCase {
        _ match {
          case Errored(e) => IO { log.error(e) }
          case _          => IO.unit
        }
      }
  }

  override def deleteDocument(id: ID): IO[Metadata] = {
    getMetadata(id).flatMap { res =>
      sql"delete from documents where id = ${id.toLong}".update.run
        .transact[IO](database) >> IO.pure(res)
    }
  }

  override def getMetadata(id: ID): IO[Metadata] = {
    Fragment(
      "select id, metadata from documents where id = ? limit 1",
      List(Arg(id, summon)),
    )
      .query[(ID, Metadata)]
      .map { (id, metadata) => metadata + ("id" -> id) }
      .unique
      .transact(database)
  }

  override def getMetadata(ids: Seq[ID]): IOStream[Metadata] = {
    NonEmptySeq.fromSeq(ids) match {
      case None => Stream.empty
      case Some(safeIds) => {
        val stmt = {
          sql"select id, metadata from documents where " ++ Fragments
            .in(fr"id", safeIds.map { _.toLong })
        }
        stmt
          .query[(ID, Metadata)]
          .map { (id, metadata) => metadata + ("id" -> id) }
          .stream
          .transact(database)
      }
    }
  }

  override def getAllMetadata(): IOStream[Metadata] = {
    sql"select id, metadata, '' from documents"
      .query[(ID, Metadata)]
      .map { (id, metadata) => metadata + ("id" -> id) }
      .stream
      .transact(database)
  }

  override def getCorpus(corpusId: ID): IOStream[Metadata] = {
    log.debug(s"Fetching corpus by id: $corpusId")
    getAllMetadata()
      .filter { metadata =>
        metadata.corpusId.filter(_ == corpusId).nonEmpty
      }

  }

  override def getContentStream(id: ID): IOStream[Byte] = {
    sql"select bytes from documents where id = ${id.toLong}"
      .query[Array[Byte]]
      .unique
      .transact[IO](database)
      .map(_.toSeq)
      |> Stream.eval
      |> { _.flatMap(Stream.emits) }
  }

  private def unsafeDeleteAll(): IO[Unit] = {
    sql"delete all from documents".update.run
      .transact(database)
      .void
  }

  override def #!(): IOStream[Metadata] = {
    getAllMetadata().onFinalizeCase {
      _ match {
        case Succeeded  => unsafeDeleteAll()
        case Errored(e) => IO.raiseError(e)
        case Canceled   => IO.canceled
      }
    }
  }
}

object SQLRepository {
  def apply()(using Config): Resource[IO, SQLRepository] = {
    val dbResource = inject[Resource[IO, Transactor[IO]]]
    dbResource.map { db => new SQLRepository(db) }
  }
}
