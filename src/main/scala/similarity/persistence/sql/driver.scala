package similarity.persistence.sql

import cats.effect.kernel.Resource
import cats.effect.IO
import doobie.hikari.*
import doobie.util.transactor.Transactor
import doobie.util.ExecutionContexts
import scala.compiletime.ops.string.+
import similarity.config.*
import similarity.environment.*
import similarity.environment.given
import similarity.inject.*
import similarity.inject.given
import similarity.logging.log
import similarity.syntax.*

type SQL[S <: String] = "SQL_" + S

private def resolveSqlDriver(url: String) = {
  url.split(":").toList match {
    case "jdbc" :: "sqlite" :: _           => "org.sqlite.JDBC"
    case "jdbc" :: "relique" :: "csv" :: _ => "org.relique.jdbc.csv.CsvDriver"
    case v =>
      throw IllegalStateException(
        s"We don't know what driver to use for ${v.mkString(":")}",
      )

  }
}

private def defaultUsername(url: String): String = {
  log.warn(s"No username set for connection '$url'!")
  ""
}

private def defaultPassword(url: String): String = {
  log.warn(s"No password set for connection '$url'!")
  ""
}

given transactorResource(using Config): Resource[IO, Transactor[IO]] = {
  val connUrl: Env[SQL["CONNECTION_URL"]] = config.persistence.sql
    .map { sqlConfig => Env.raw[SQL["CONNECTION_URL"]](sqlConfig.url) }
    .getOrElse {
      inject[Env[SQL["CONNECTION_URL"]]]
    }
  val username = inject[Env[SQL["USERNAME"]]]
  val password = inject[Env[SQL["PASSWORD"]]]
  val url = connUrl.value
  def driverClassName = resolveSqlDriver(url)

  for {
    threadPool <- ExecutionContexts.fixedThreadPool[IO](16)
    cp <-
      HikariTransactor.newHikariTransactor[IO](
        driverClassName = driverClassName,
        url = url,
        user = username.valueOpt.getOrElse { defaultUsername(url) },
        pass = password.valueOpt.getOrElse { defaultPassword(url) },
        connectEC = threadPool,
      )
  } yield cp
}
