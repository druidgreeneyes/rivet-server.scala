package similarity

import cats.effect.kernel.Resource
import cats.effect.unsafe.IORuntime
import cats.effect.IO
import io.circe.Codec
import similarity.config.*
import similarity.inject.*
import similarity.inject.given
import similarity.logging.log
import similarity.model.*
import similarity.persistence.hashmap.*
import similarity.persistence.mongodb.*
import similarity.persistence.sql.SQLRepository

package object persistence {

  trait DocumentRepository {
    def getAllMetadata(): IOStream[Metadata]
    def getCorpus(id: ID): IOStream[Metadata]
    def getDocument(id: ID): IO[DocumentEntity]
    def getContentStream(id: ID): IOStream[Byte]
    def getMetadata(id: ID): IO[Metadata]
    def getMetadata(ids: Seq[ID]): IOStream[Metadata]
    def saveDocument(thing: DocumentEntity): IO[Metadata]
    def saveDocuments(things: IOStream[DocumentEntity]): IOStream[Metadata]
    def deleteDocument(id: ID): IO[Metadata]
    def #!(): IOStream[Metadata]
  }

  object DocumentRepository {
    given injector(using
        Config,
        IORuntime,
    ): Resource[IO, DocumentRepository] = {
      val backend = config.persistence.backend
      log.info(s"Using Persistence Backend: ${backend}")
      backend match {
        case Backend.HashMap => HashMapRepository()
        case Backend.MongoDB => MongoDBRepository()
        case Backend.SQL     => SQLRepository()
      }
    }
  }
}
