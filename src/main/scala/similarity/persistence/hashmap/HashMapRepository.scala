package similarity.persistence.hashmap

import cats.effect.std.AtomicCell
import cats.effect.unsafe.IORuntime
import cats.effect.IO
import cats.effect.Resource
import cats.syntax.applicativeError.*
import cats.syntax.functorFilter.*
import cats.syntax.traverse.*
import fs2.Stream
import java.util.UUID
import mouse.any.*
import scala.collection.concurrent.TrieMap
import scala.concurrent.duration.*
import similarity.config.Config
import similarity.logging.log
import similarity.model.*
import similarity.model.Metadata.*
import similarity.persistence.*
import similarity.syntax.functor.*
import similarity.syntax.option.*
import similarity.logging.log

class HashMapRepository private (
    private val store: TrieMap[String, DocumentEntity],
) extends DocumentRepository {

  override def getAllMetadata(): IOStream[Metadata] = {
    for {
      metadata <- Stream.iterable(store.values.map { _.metadata })
    } yield metadata
  }

  override def getCorpus(corpusId: ID): IOStream[Metadata] =
    getAllMetadata().filter { metadata => metadata.corpusId == some(corpusId) }

  override def getDocument(id: ID): IO[DocumentEntity] = {
    log.debug(s"Reading id $id from store $store")
    IO.pure(store(id))
  }

  override def getMetadata(id: ID): IO[Metadata] =
    IO.pure(store(id).metadata)

  override def getMetadata(ids: Seq[ID]): IOStream[Metadata] =
    ids.map(getMetadata).sequence |> Stream.evals

  override def getContentStream(id: ID): IOStream[Byte] =
    Stream.emits(store(id).bytes)

  private def newId(): IO[ID] = {
    Stream
      .repeatEval(IO { UUID.randomUUID().toString() })
      .filterNot(store.contains)
      .head
      .compile
      .onlyOrError
      .timeout(5.seconds)
  }

  private def setId(thing: DocumentEntity): IO[(ID, DocumentEntity)] = {
    thing.metadata.id match {
      case None => {
        newId()
          .map { id =>
            id -> thing.updateMetadata("id" -> id)
          }
      }
      case Some(id) => IO.pure(id -> thing)
    }
  }

  override def saveDocument(thing: DocumentEntity): IO[Metadata] = {
    for {
      kv <- setId(thing)
      (id, toSave) = kv
    } yield {
      store.update(id, toSave)
      store(id).metadata
    }
  }

  override def saveDocuments(
      things: IOStream[DocumentEntity],
  ): IOStream[Metadata] = things.evalMap(saveDocument)

  override def deleteDocument(id: ID): IO[Metadata] =
    IO.pure(store.remove(id).get.metadata)

  override def #!(): IOStream[Metadata] = {
    val old = store.values.map { _.metadata }
    store.clear()
    Stream.iterable(old)
  }
}

object HashMapRepository {
  private lazy val instance = new HashMapRepository(TrieMap())
  def apply()(using IORuntime): Resource[IO, HashMapRepository] = {
    Resource.pure(instance)
  }
}
