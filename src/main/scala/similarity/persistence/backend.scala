package similarity.persistence

import io.circe.Decoder
import io.circe.HCursor

enum Backend {
  case HashMap extends Backend
  case MongoDB extends Backend
  case SQL extends Backend
}

object Backend {
  given backendDecoder: Decoder[Backend] with {
    override def apply(c: HCursor): Decoder.Result[Backend] = {
      Decoder.decodeString(c).map(Backend.valueOf)
    }
  }
}
