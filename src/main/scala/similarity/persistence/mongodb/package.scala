package similarity.persistence

import similarity.environment.*
import similarity.exception.PublicException
import similarity.model.*

package object mongodb {
  type ConnectionUrl = Env["MONGODB_CONNECTION_URL"]
  type DatabaseName = Env["MONGODB_DATABASE_NAME"]

  class MongoDBInsertionFailure(entity: DocumentEntity)
      extends PublicException(
        s"Unable to insert entity with id ${entity.metadata.id}!",
      )
}
