package similarity.persistence.mongodb

import cats.effect.kernel.MonadCancel
import cats.effect.kernel.Resource
import cats.effect.kernel.Resource.ExitCase.Canceled
import cats.effect.kernel.Resource.ExitCase.Errored
import cats.effect.kernel.Resource.ExitCase.Succeeded
import cats.effect.unsafe.IORuntime
import cats.effect.IO
import com.mongodb.bulk.BulkWriteInsert
import com.mongodb.client.model.WriteModel
import com.mongodb.internal.bulk.WriteRequest
import fs2.interop.reactivestreams as RStream
import fs2.Stream
import io.circe.parser
import io.circe.Codec
import mouse.any.*
import org.bson.types.ObjectId
import org.mongodb.scala.bson.collection.immutable.Document
import org.mongodb.scala.bson.BsonElement
import org.mongodb.scala.model.Filters
import org.mongodb.scala.model.InsertOneModel
import org.mongodb.scala.model.Projections
import org.mongodb.scala.MongoClient
import org.mongodb.scala.MongoCollection
import org.mongodb.scala.Observable
import scala.language.implicitConversions
import similarity.config.Config
import similarity.environment.*
import similarity.environment.given
import similarity.inject.*
import similarity.inject.given
import similarity.logging.log
import similarity.model.*
import similarity.persistence.*
import similarity.syntax.either.*
import similarity.syntax.functor.*

class MongoDBRepository private (private val coll: MongoCollection[Document])
    extends DocumentRepository {

  override def getAllMetadata(): IOStream[Metadata] = {
    coll.find().map { _.asEntity.metadata }.asIOStream
  }

  override def getCorpus(corpusId: ID): IOStream[Metadata] = {
    coll
      .find(Filters.eq("metadata.corpusId", corpusId))
      .projection(Projections.include("metadata"))
      .map { _.asMetadata }
      .asIOStream
  }

  override def getContentStream(id: ID): IOStream[Byte] = {
    Stream.eval(getDocument(id)).flatMap(d => Stream.emits(d.bytes))
  }

  override def getDocument(id: ID): IO[DocumentEntity] = {
    coll.find(Filters.eq("_id", id)).first().asIO.map { _.asEntity }
  }

  override def getMetadata(id: ID): IO[Metadata] = getMetadata(
    Seq(id),
  ).compile.onlyOrError
  override def getMetadata(ids: Seq[ID]): IOStream[Metadata] = {
    coll
      .find(Filters.in("_id", ids))
      .projection(Projections.include("metadata"))
      .map { _.asMetadata }
      .asIOStream
  }

  override def saveDocument(thing: DocumentEntity): IO[Metadata] = {
    coll.insertOne(thing.asDocument).asIO.flatMap { res =>
      if (res.wasAcknowledged()) {
        IO.pure(thing.metadata)
      } else {
        IO.raiseError(MongoDBInsertionFailure(thing))
      }
    }
  }

  override def saveDocuments(
      things: IOStream[DocumentEntity],
  ): IOStream[Metadata] = {
    things.chunkAll
      .flatMap { chunk =>
        chunk.to(Seq).map { e => InsertOneModel(e.asDocument) }
          |> { coll.bulkWrite(_).asIOStream }
      }
      .flatMap { _ => things }
      .map { _.metadata }
  }

  override def deleteDocument(id: ID): IO[Metadata] = {
    for {
      doc <- getDocument(id)
      res <- coll.deleteOne(Filters.eq("id", id)).asIO
    } yield doc.metadata
  }

  override def #!(): IOStream[Metadata] = {
    getAllMetadata().onFinalizeCase {
      _ match {
        case Succeeded  => coll.deleteMany(Filters.empty()).asIO.void
        case Errored(e) => IO.raiseError(e)
        case Canceled   => IO.canceled
      }
    }
  }
}

object MongoDBRepository {
  def apply()(using Config): Resource[IO, MongoDBRepository] = {
    val dbName = inject[DatabaseName]
    val connUrl = inject[ConnectionUrl]
    Resource
      .fromAutoCloseable(IO { MongoClient(uri = connUrl.value) })
      .map { _.getDatabase(dbName.value).getCollection[Document]("documents") }
      .map { new MongoDBRepository(_) }
  }
}
