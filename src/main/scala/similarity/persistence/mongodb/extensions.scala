package similarity.persistence.mongodb

import cats.effect.IO
import fs2.interop.reactivestreams as RStream
import mouse.any.*
import org.bson.types.ObjectId
import org.mongodb.scala.bson.collection.immutable.Document
import org.mongodb.scala.Observable
import similarity.model.*

extension (doc: Document) {
  def asEntity: DocumentEntity = {
    val bytes = doc("bytes").asBinary().getData()
    val metadata = (doc - "bytes").asMetadata
    DocumentEntity(metadata, bytes)
  }
  def asMetadata: Metadata = {
    doc.map { (k, v) => (k, v.toString) } |> Metadata.apply
  }
}

extension (e: DocumentEntity) {
  def asDocument: Document = {
    val metadata = e.metadata.id match {
      case None => e.metadata
      case Some(id) =>
        e.metadata ++ Map(
          "_id" -> ObjectId(id).toString,
        )
    }

    metadata.foldLeft(Document()) { (doc, kv) =>
      doc + kv
    } + ("bytes" -> e.bytes)
  }
}

extension [T](obs: Observable[T]) {
  def asIOStream(bufferSize: Int): IOStream[T] =
    RStream.fromPublisher(obs, bufferSize)
  def asIOStream: IOStream[T] = asIOStream(1)
  def asIO: IO[T] = IO.fromFuture(IO(obs.head))
}
