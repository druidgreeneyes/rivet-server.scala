package similarity.persistence

import io.circe.Codec
import scala.compiletime.*
import scala.compiletime.testing.*
import scala.deriving.Mirror
import scala.quoted.*
import scala.reflect.ClassTag
import similarity.model.Metadata
import similarity.syntax.*

opaque type ClassName[T] = String
object ClassName {
  given derived[T](using ct: ClassTag[T]): ClassName[T] = {
    ct.runtimeClass.getSimpleName()
  }
}
