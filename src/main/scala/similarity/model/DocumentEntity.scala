package similarity.model

import com.github.druidgreeneyes.rivet.core.labels.RIV
import similarity.syntax.*

case class DocumentEntity(
    val metadata: Metadata,
    val bytes: Array[Byte],
)

object DocumentEntity {

  def apply(
      metadata: Metadata,
      text: String,
  ): DocumentEntity = {
    DocumentEntity(
      metadata,
      text.getBytes(CHARSET),
    )
  }

  extension (e: DocumentEntity) {
    def id: Option[ID] = e.metadata.id
    def riv: Option[RIV] = e.metadata.riv
    def withMetadata(m: Metadata): DocumentEntity = e.copy(metadata = m)
    def updateMetadata(updates: (String, String)*): DocumentEntity = {
      e.copy(metadata = e.metadata ++ updates)
    }
  }
}
