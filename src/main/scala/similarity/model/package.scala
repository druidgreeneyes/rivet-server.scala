package similarity

import java.nio.charset.StandardCharsets

package object model {
  type ID = String
  type Result[T] = Either[String, T]
  type IOStream[T] = fs2.Stream[cats.effect.IO, T]
  val CHARSET = StandardCharsets.UTF_8
  export Metadata.*
}
