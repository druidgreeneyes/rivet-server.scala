package similarity.model

import com.github.druidgreeneyes.rivet.core.labels.ArrayRIV
import com.github.druidgreeneyes.rivet.core.labels.RIV
import io.circe.*
import mouse.any.*
import similarity.logging.log
import sttp.tapir.Schema

opaque type Metadata = Map[String, String]

object Metadata {
  def apply(m: Map[String, String]): Metadata = m
  def apply(elems: Iterable[(String, String)]): Metadata = elems.toMap
  def apply(elems: (String, String)*): Metadata = apply(elems)

  given metadataEncoder: Encoder[Metadata] = Encoder.encodeJson.contramap { m =>
    m.view.mapValues(Json.fromString).toSeq |> Json.obj
  }

  given metadataDecoder: Decoder[Metadata] = {
    Decoder.decodeJsonObject.map {
      _.toIterable.map { (k, v) => (k, v.asString.get) }.toMap |> Metadata.apply
    }
  }

  given metadataSchema: Schema[Metadata] = Schema.anyObject

  extension (m: Metadata) {
    def get(key: String): Option[String] = m.get(key)
    def ++(other: IterableOnce[(String, String)]): Metadata = m ++ other
    def ++(other: Metadata): Metadata = m ++ other
    def +(kv: (String, String)): Metadata = m + kv
    def -(key: String): Metadata = m - key

    def view = m.view
    def foldLeft[W] = m.foldLeft[W]
    def toSeq = m.toSeq

    def riv: Option[RIV] = m.get("riv").map(ArrayRIV.fromString)
    def id: Option[ID] = m.get("id")
    def contentType = m.get("contentType")
    def corpusId: Option[String] = m.get("corpusId")

    def json = metadataEncoder(m)
    def jsonStr = m.json.noSpacesSortKeys
  }
}
