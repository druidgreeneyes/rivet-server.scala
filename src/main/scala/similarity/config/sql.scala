package similarity.config

import scala.quoted.*

case class SQLConfig(
    val url: String,
)

object SQLConfig {
  given toExpr: ToExpr[SQLConfig] with {
    override def apply(x: SQLConfig)(using Quotes): Expr[SQLConfig] = '{
      SQLConfig(${ Expr(x.url) })
    }
  }
}
