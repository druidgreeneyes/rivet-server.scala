package similarity.config

import scala.quoted.*
import similarity.persistence.Backend

case class PersistenceConfig(
    val backend: Backend = Backend.HashMap,
    val sql: Option[SQLConfig] = None,
)

object PersistenceConfig {
  given toExpr: ToExpr[PersistenceConfig] with {
    override def apply(
        x: PersistenceConfig,
    )(using Quotes): Expr[PersistenceConfig] = '{
      PersistenceConfig(
        backend = ${ Expr(x.backend) },
      )
    }
  }

  given backendToExpr: ToExpr[Backend] with {
    override def apply(x: Backend)(using Quotes) = {
      '{ Backend.fromOrdinal(${ Expr(x.ordinal) }) }
    }
  }
}
