package similarity

import cats.effect.IO
import cats.syntax.all.*
import io.circe.*
import io.circe.generic.auto.*
import io.circe.yaml
import io.circe.Decoder.Result
import java.nio.file.*
import mouse.all.*
import scala.quoted.*
import similarity.persistence.*
import similarity.util.file.FileType

package object config {
  case class Config(
      persistence: PersistenceConfig = PersistenceConfig(),
      env: EnvConfig = EnvConfig(),
  )

  def config(using cfg: Config) = cfg

  object Config {

    given toExpr: ToExpr[Config] with {
      override def apply(c: Config)(using Quotes): Expr[Config] = '{
        Config(
          persistence = ${ Expr(c.persistence) },
          env = ${ Expr(c.env) },
        )
      }
    }

    val configFilePath = {
      FileType.YAML.resolve("src/main/resources/application")
    }

    private def convert(json: Either[ParsingFailure, Json]): Config = {
      json.flatMap { _.as[Config] }.valueOr { err =>
        err.printStackTrace()
        throw err
      }
    }

    private def configExpr(using Quotes): Expr[Config] =
      Files.readString(configFilePath) |> yaml.parser.parse
        |> convert
        |> { Expr(_) }

    private[similarity] inline def load: Config = ${ configExpr }
  }
}
