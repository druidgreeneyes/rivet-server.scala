package similarity.config

import scala.quoted.*

case class EnvConfig(
    val prefix: String = "APP",
)

object EnvConfig {

  given ToExpr[EnvConfig] with {
    override def apply(e: EnvConfig)(using Quotes): Expr[EnvConfig] = {
      '{ EnvConfig(prefix = ${ Expr(e.prefix) }) }
    }
  }
}
