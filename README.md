## Demo RIV server in scala

### Usage

Start the server with `sbt run`, then add documents by posting them to `localhost:8096/document`:

```sh
$ curl -F fileName=firstFile -F file=@firstFile.txt localhost:8096/document
{"id":"1f31e7f5-8349-4853-beee-b7741d8c22e4","fileName":"firstFile"}
$ curl -F fileName=secondFile -F file=@secondFile.txt localhost:8096/document
{"id":"d5e2b4b3-e4d8-4d69-a458-5c51e7c600fc","fileName":"secondFile"}
```

Then invoke a comparison using the `compare` endpoint:

```sh
$ curl -G -d docA=1f31e7f5-8349-4853-beee-b7741d8c22e4 -d docB=d5e2b4b3-e4d8-4d69-a458-5c51e7c600fc localhost:8096/compare | jq .
{
  "documentA": {
    "id": "1f31e7f5-8349-4853-beee-b7741d8c22e4",
    "fileName": "readme"
  },
  "documentB": {
    "id": "d5e2b4b3-e4d8-4d69-a458-5c51e7c600fc",
    "fileName": "readme"
  },
  "cosineSimilarity": 1.0
}
# Hey these two texts are the same!
```

You can show the text of a given file as well:

```sh
$ curl -G -d id=1f31e7f5-8349-4853-beee-b7741d8c22e4 localhost:8096/document
# wall of text
```

### NOTE

This is a quick POC so it stores the files in memory. Don't overdo it. Adding a database or other storage layer should be pretty easy.

## Development

This is a normal sbt project. You can compile code with `sbt compile`, run it with `sbt run`, and `sbt console` will start a Scala 3 REPL.

In order to start and use MongoDB you can use the scripts in `sbin`:

```sh
$ sbin/start-mongodb.sh
5dc168b2-d530-4812-8249-67918ff64d7b
$ source sbin/mongodb.env
$ env | grep RIVET
RIVETMONGODB_CONNECTION_URL=mongodb://127.0.0.1:27017/?serverSelectionTimeoutMS=2000
RIVETMONGODB_DATABASE_NAME=rivet
$ sbt clean run
[ ... ]
2023.05.11 20:41:31:115 io-compute-16 INFO org.http4s.blaze.channel.nio1.NIO1SocketServerGroup
    Service bound to address /127.0.0.1:8096
2023.05.11 20:41:31:147 blaze-acceptor-0-0 DEBUG org.http4s.blaze.channel.nio1.SelectorLoop
    Channel initialized.
2023.05.11 20:41:31:147 io-compute-16 INFO org.http4s.blaze.server.BlazeServerBuilder
    
      _   _   _        _ _
     | |_| |_| |_ _ __| | | ___
     | ' \  _|  _| '_ \_  _(_-<
     |_||_\__|\__| .__/ |_|/__/
                 |_|
2023.05.11 20:41:31:165 io-compute-16 INFO org.http4s.blaze.server.BlazeServerBuilder
    http4s v0.23.18 on blaze v0.23.14 started at http://127.0.0.1:8096/
```

### Project Structure

Basic structure of the app is the API functionality (endpoints that do stuff) are grouped by umbrella features (for example, currently we have a `Hello` feature and a `RIV` feature). All the logic that is specific to a feature lives in that feature's package, and all feature packages live in the `app` module, so:

```
src/main/scala/similarity
+- [...]
+- app
   +- [...]
   +- hello
      +- [ hello feature endpoints and logic ]
   +- riv
      +- [ riv feature endpoints and logic ]
```

Common logic that is shared between features lives in its own packages under the similarity root, so:

```
src/main/scala/similarity
+- config
   +- [ logic for reading configuration from `src/main/resources/application.yml` at compile time ]
+- environment
   +- [ logic for reading environment variables at runtime ]
+- inject
   +- [ logic for implementing dependency injection ]
+- logging
   +- [ logic for writing and formatting log messages ]
+- util
   +- [ miscellaneous other stuff ]
```

The distinction between what lives in the similarity root and what lives in `util` is fairly arbitrary, but I have generally been putting things in the similarity root specifically when they are used not just in the `app` package but in many other places in `util` as well. For example, a `syntax` package that provides infix operators that are in turn used everywhere would get promoted to the similarity root package.
