#!/bin/sh

sqlite3 \
  -header \
  -csv \
  -readonly \
  target/data/test.sqlite.db \
  "select * from documents;" \
  > target/data/documents.csv 
