# Developer Documentation

This is a normal sbt project. You can compile the code with `sbt compile`, run it with `sbt run`, and `sbt console` will start a Scala 3 REPL.

In order to start and use any given persistence backend, use the scripts in `sbin`:

```sh
$ sbt clean run
[ ... ]
2023.05.11 20:41:31:115 io-compute-16 INFO org.http4s.blaze.channel.nio1.NIO1SocketServerGroup
    Service bound to address /127.0.0.1:8096
2023.05.11 20:41:31:147 blaze-acceptor-0-0 DEBUG org.http4s.blaze.channel.nio1.SelectorLoop
    Channel initialized.
2023.05.11 20:41:31:147 io-compute-16 INFO org.http4s.blaze.server.BlazeServerBuilder
    
      _   _   _        _ _
     | |_| |_| |_ _ __| | | ___
     | ' \  _|  _| '_ \_  _(_-<
     |_||_\__|\__| .__/ |_|/__/
                 |_|
2023.05.11 20:41:31:165 io-compute-16 INFO org.http4s.blaze.server.BlazeServerBuilder
    http4s v0.23.18 on blaze v0.23.14 started at http://127.0.0.1:8096/
```

### Project Structure

Basic structure of the app is the API functionality (endpoints that do stuff) are grouped by umbrella features (for example, currently we have a `Hello` feature and a `RIV` feature). All the logic that is specific to a feature lives in that feature's package, and all feature packages live in the `app` module, so:

```
src/main/scala/similarity
+- [...]
+- app
   +- [...]
   +- hello
      +- [ hello feature endpoints and logic ]
   +- riv
      +- [ riv feature endpoints and logic ]
```

Common logic that is shared between features lives in its own packages under the similarity root, so:

```
src/main/scala/similarity
+- config
   +- [ logic for reading configuration from `src/main/resources/application.yml` at compile time ]
+- environment
   +- [ logic for reading environment variables at runtime ]
+- inject
   +- [ logic for implementing dependency injection ]
+- logging
   +- [ logic for writing and formatting log messages ]
+- util
   +- [ miscellaneous other stuff ]
```

The distinction between what lives in the similarity root and what lives in `util` is fairly arbitrary, but I have generally been putting things in the similarity root specifically when they are used not just in the `app` package but in many other places in `util` as well. For example, a `syntax` package that provides infix operators that are in turn used everywhere would get promoted to the similarity root package.
