### Project Structure

Basic structure of the app is the API functionality (endpoints that do stuff) are grouped by umbrella features (for example, currently we have a `Hello` feature for basic dev testing, a `Document` feature for performing crud ops on documents (upload, read, etc), and an `Ops` feature for computation and analysis). All the logic that is specific to a feature lives in that feature's package, and all feature packages live in the `app` module, so:

```
src/main/scala/similarity
+- [...]
+- app
   +- [...]
   +- hello
      +- [ hello feature endpoints and logic ]
   +- riv
      +- [ riv feature endpoints and logic ]
```

Common logic that is shared between features lives in its own packages under the similarity root, so:

```
src/main/scala/similarity
+- config
   +- [ logic for reading configuration from `src/main/resources/application.yml` at compile time ]
+- environment
   +- [ logic for reading environment variables at runtime ]
+- inject
   +- [ logic for implementing dependency injection ]
+- logging
   +- [ logic for writing and formatting log messages ]
+- util
   +- [ miscellaneous other stuff ]
```

The distinction between what lives in the similarity root and what lives in `util` is fairly arbitrary, but I have generally been putting things in the similarity root specifically when they are used not just in the `app` package but in many other places in `util` as well. For example, a `syntax` package that provides infix operators that are in turn used everywhere would get promoted to the similarity root package.

### Dependency Injection

There's no SpringBoot or similar framework here, so we have a much-stripped down version of dependency injection, the logic for which can be found in the `inject` package. Broadly speaking, there are two ways available to you for handling class/object dependencies, `inject[T]` calls, or `(using t: T)` parameters. `using` parameters are built into the language and are the basis for most of the `inject` logic, so I'll start there. Let's say I have a service `FooService` that needs a database connection `DB`. I can start by making that a parameter of the `FooService` class:

```scala
class FooService(private val db: DB) {
  def getFoo(id: FooID): Option[Foo] = db.getById(id)
}
```

But of course that means I now have to explicitly pass that parameter down from the toplevel of the app:

```scala
class FooController(private val db: DB) {
  private val svc = FooService(db)
}
```

Or I have to explicitly create the service and pass it around instead:

```scala
object main {
  val fooDB = DB(???)
  val fooSvc = FooServic(fooDB)
  val fooController = FooController(fooService)
  def main(args: String*): Unit = {
    ???
  }
}
```

We all know that gets old fast.

#### Summoning a given value

Instead we can use scala3's `given` and `using` tools:

```scala
// file: app/foo/db.scala
object db {
  trait DB {
    def getById(id: FooID): Option[Foo]
  }

  object DBImpl extends DB {
    ???
  }

  given fooDB: DB = DBImpl
}

// file: app/foo/service.scala
import app.foo.db.DB

object service {
  trait FooService {
    def getFoo(id: FooID): Option[Foo]
  }

  class FooServiceImpl(private val db: DB) extends FooService {
    override def getFoo(id: FooID) = db.getById(id)
  }

  given fooSvc(using db: DB): FooService = FooServiceImpl(db)
}

// file: app/foo/controller.scala
import app.foo.service.FooService

object controller {
  trait FooController {
    ???
  }

  class FooControllerImpl(private val svc: FooService) extends FooController {
    ???
  }

  given fooController(using svc: FooService): FooController = FooControllerImpl(svc)
}

// file app/main.scala

import app.foo.db.given
import app.foo.service.given
import app.foo.controller.given
import app.foo.controller.FooController

object main {
  val fooController = summon[FooController]

  def main(args: String*): Unit = {
    ???
  }
}
```

`summon` is a function that just pulls a `given` value out of the available context. It's implemented like this:

```scala
def summon[T](using t: T): T = t
```

As long as there's a `given T` available in the current context (either locally defined or through the import statements at the top of the file), `summon` will provide it. `given` instance can be functions, and they can include `using` parameters themselves, in which case each of *those* parameters also needs to be available at the point where that given is `summon`-ed. So in this case, we still need to import all the `given`s from our entire heirarchy so that we can `summon` a `FooController` in `main`. If we don't want to pass that need up the chain, we can rewrite our various classes as follows:

```scala
// file: app/foo/db.scala
object db {
  trait DB {
    def getById(id: FooID): Option[Foo]
  }

  object DBImpl extends DB {
    ???
  }

  given fooDB: DB = DBImpl
}

// file: app/foo/service.scala
import app.foo.db.DB
import app.foo.db.given

object service {
  trait FooService {
    def getFoo(id: FooID): Option[Foo]
  }

  class FooServiceImpl(private val db: DB) extends FooService {
    override def getFoo(id: FooID) = db.getById(id)
  }

  given fooSvc: FooService = {
    val db = summon[DB]
    FooServiceImpl(db)
  }
}

// file: app/foo/controller.scala
import app.foo.service.FooService
import app.foo.service.given

object controller {
  trait FooController {
    ???
  }

  class FooControllerImpl(private val svc: FooService) extends FooController {
    ???
  }

  given fooController: FooController = {
    val svc = summon[FooService]
    FooControllerImpl(svc)
  }
}

// file app/main.scala

import app.foo.controller.given
import app.foo.controller.FooController

object main {
  val fooController = summon[FooController]

  def main(args: String*): Unit = {
    ???
  }
}
```

Note that now we only have to worry about the dependency *at the point where that dependency is used*, which is pretty nice!

#### Injecting a value

Use `inject` when you need to provide for a little bit more logic around your given; for example:


```scala
object db {
  trait DB {
    ???
  }

  object DBInjector extends Injector[DB] {
    private lazy val db = DBImpl()
    override def apply() = db
  }

  given dbInjector: Injector[DB] = DBInjector
}
```

Or when you want a shortcut for some commonly used injection behavior:

```scala
object myService {
  object ServiceInjector extends Injector[Resource[IO, MyService]] {
    private val dbResource = injectResource[DB]
    override def apply() = dbResource.map{db => new MyService(db)}
  }
}
```

#### Which one should I use and when?

General rules of thumb:

- Use `given/using` where it doesn't impose too much overhead.
- Use `inject[T]` otherwise, or where you need to include some intermediary logic around the `Injector`.
- Use `injectResource[T]` where you are either using in place or providing upstream something that needs to be allocated and cleaned up. This is pretty common, so expect to see and use `injectResource[T]` all over the place.

Gotchas:

- `inject[T]` will need an instance of `Injector[T]` or just a `T` to be available in the context *at compile time*, which means that using them in generic code that might not know exactly what `T` is at compile time can be tricky. Where possible, avoid summoning or injecting a generic type, and instead pass it down through `using` parameters.
- Likewise, `injectResource[T]` will need an instance of `Injector[Resource[IO, T]]` or a `Resource[IO, T]` to be in scope, so all the caveats above apply here too.
