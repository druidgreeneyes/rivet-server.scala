# Asynchrony and Resource Management

This application is built entirely around [cats-effect](https://typelevel.org/cats-effect/). The relational database logic uses it, the http server logic uses it, it's everywhere. As such, you can expect to see and use a couple of things on a regular basis:

## The IO Monad

An application build using cats-effect uses the `IO` type to encapsulate almost everything. At its core, `IO` represents an operation that may fail, and encapsulates that failure so that it can be passed up the call stack and handled elsewhere without intervening code having to think about it (beyond wrapping everything in `IO`). The most important things to know about `IO` are:

### IO Represents Intent, not Action

Say I want to start using `IO` in my application that, heretofore, just uses normal functions. The obvious place to start would be to do this:

```scala
// This is my application
def main(): Int = {
    new Bar().getDrinks().getAlcoholContent()
}

// But running getDrinks() may fail, so we wrap our application in IO:
def main(): IO[Int] = IO {
    new Bar().getDrinks().getAlcoholContent()
}
```

But when I run that, nothing happens. Previously, the application did stuff; now it just exits. Why? Because IO represents Intent, not Action. Think about if you were doing this in person. In the first case, you go to a bar, you get some drinks, and then you estimate how drunk you are. In the second case (wrapped in `IO`), you go to your friend and you say "I'm going to go to a bar, get some drinks, and then estimate how drunk I am." And then you both move on with your day. `IO` is your *intent* to do something. Nothing is actually done until someone asks for a result from `IO`. Some time later, your friend goes "Hey, so how drunk are you?" You immediately dash to the nearest bar, chug whatever they have on hand, dash back, and say "Ghg?" Likewise with `IO`, we have to actually ask for a result before any of the logic inside is actually invoked:

```scala
def main(): Int = IO {
    new Bar().getDrinks().getAlcoholContent()
}.unsafeRunSync()
```

### IO is a Monad

So why do that? It's obviously unsafe! Well, the short answer is that in the simple case `IO` isn't really worth the overhead it imposes. But in a complex case, `IO` is a monad, which primarily means it has `map` and `flatMap` operations. This in turn means we can do all kinds of fun things with composition and `for` comprehensions.

Let's say you and your friend plan to go to the bar together, then compare who is more drunk. But at any point during this process either or both of you could just fall over and be unable to finish:

```scala
class Bar() {
    def getDrinks(howMany: Int = 3): IO[Drinks] = ???
}

class Drinks() {
    def getAlcoholContent(): IO[Int] = ???
}

def goForDrinks(who: String, bar: Bar): IO[Int] = {
    for {
        drinks <- bar.getDrinks()
        drunk <- drinks.getAlcoholContent()
    } yield (who, drunk)
}

def main(): Int = {
    val bar = new Bar()

    val whoIsDrunker: IO[(String, Int)] = for {
        me <- goForDrinks("me", bar)
        friend <- goForDrinks("friend", bar)
    } yield List(me, friend).maxBy{(_, drunk) -> drunk}

    whoIsDrunker.unsafeRunSync()
}
```

The syntax above is known as [for comprehension](https://docs.scala-lang.org/tour/for-comprehensions.html), and it's a piece of syntactic sugar around objects that can do `map` and `flatMap` operations. Being a monad, `IO` fits the bill. To show what's going on, we could rewrite the `goForDrinks` function as follows:

```scala
def goForDrinks(who: String, bar: Bar, howMany: Int = 3): IO[Int] = {
    val drinksIO: IO[Drinks] = bar.getDrinks(howMany)
    val drunkIO: IO[Int] = drinksIO.flatMap{drinks => drinks.getAlcoholContent()}

    drunkIO.flatMap{drunk => (who, drunk)}
}
```

Same result, different way of writing it. Or let's say you and your friend will go to different bars, each get 5 drinks, and record how drunk the person is who gets back first. `IO` is asynchronous, so we can do it like this:

```scala
def main(): (String, Int) = {
    val meIO = goForDrinks("me", new Bar(), 5)
    val friendIO = goForDrinks("friend", new Bar(), 5)
    val whoFinishedFirstIO = IO.race(meIO, friendIO)

    whoFinishedFirstIO.unsafeRunSync()
}
```

Remember, nothing happens in `IO` until someone asks for a result; but `IO` lets us compose asynchronous, effectful computations in a lot of ways that are clean and easy to read. But we keep using `unsafeRunSync()` everywhere! That's fine for demo code, but in a real application, we ought not to be doing that if we can avoid it. The good new is, we can; `cats-effect` provides a wrapper around `main()` that does the grunt work of handling `IO` for us:

```scala
import cats.effect.ExitCode
import cats.effect.IO
import cats.effect.IOApp

object Main extends IOApp {
    def run(args: List[String]): IO[ExitCode] = {
        val meIO = goForDrinks("me", new Bar(), 5)
        val friendIO = goForDrinks("friend", new Bar(), 5)

        for {
            whoFinishedFirst <- IO.race(meIO, friendIO)
        } yield {
            val (who, drunk) = whoFinishedFirst
            println(s"Finished: $who, drunk: $drunk")
            ExitCode.Success
        }
    }
}
```

## But we're not doing that, exactly

Because we have managed resources and cleanup to think about, and `cats-effect` conveniently provides a wrapper around `IO` for just that purpose. It's called `Resource`.

### Automatic Resource Management

Scala doesn't have a build-in syntax for resource management like Java's [try-with-resources](https://docs.oracle.com/javase/tutorial/essential/exceptions/tryResourceClose.html). [Resource](https://typelevel.org/cats-effect/docs/std/resource) is `cats-effect`'s answer to that. Let's say you and your friend are going to your respective liquor cabinets instead of out to bars (you know, to save money by drinking the good stuff at home). But the liquor cabinet shouldn't just be left open all the time because some liquors will degrade in the presence of sunlight. So you need to go the cabinet, open it, down five drinks, close it again, and then go back to your meeting place. `Resource` gives us a convenient way to make sure the liquor cabinet only stays open for as long as we need it to:

```scala
import cats.effect.kernel.Resource

class LiquorCabinet(val owner: String) {
    def getDrinks(howMany: Int = 3): IO[Drinks] = ???
    def close(): IO[Unit] = doCloseCabinetDoor
}

object LiquorCabinet {
    def get(owner: String): Resource[IO, LiquorCabinet] = Resource.make(
        acquire = IO { new LiquorCabinet(owner) }
    ) (
        release = {liquorCabinet => liquorCabinet.close()}
    )
}


def goForDrinks(liquorCabinet: LiquorCabinet, howMany: Int = 3): IO[(String, Int)] = {
    for {
        drinks <- liquorCabinet.getDrinks(howMany)
        drunk <- drinks.getAlcoholContent()
    } yield (liquorCabinet.owner, drunk)
}


object Main extends IOApp {
    def run(args: List[String]): IO[ExitCode] = {
        val myLiquorCabinetResource = LiquorCabinet.get("me")
        val friendLiquorCabinetResource = LiquorCabinet.get("friend")

        val meIO = myLiquorCabinetResource.use { goForDrinks(_, 5) }
        val friendIO = friendLiquorCabinetResource.use { goForDrinks(_, 5) }

        for {
            whoFinishedFirst <- IO.race(meIO, friendIO)
        } yield {
            val (who, drunk) = whoFinishedFirst
            println(s"Finished: $who, drunk: $drunk")
            ExitCode.Success
        }
    }
}
```

And voila! The liquor cabinets are automatically closed for you. You can conveniently create a resource around basically anything, and specifically `Resource` provides a function for wrapping anything that implements `java.util.Closeable()`:

```scala
val inputStream: InputStream = ???
val inputStreamResource = Resource.fromAutoCloseable(inputStream)
inputStreamResource.use {is => 
  doSomeStuff(is)
}
```

### Long-lived stuff

But this starts to break down a little when we have to start dealing with something that we need to cleanup on application shutdown, but that should otherwise stick around for the lifetime of the application. We end up having to do something like this:

```scala
object Main extends IOApp {
    val dbResource = DB.get()

    def runApp(db: DB): IO[ExitCode] = ???

    def run(args: List[String]): IO[ExitCode] = {
        dbResource.use { db => 
            runApp(db)
        }
    }
}
```

Which is alright when you only have one resource to deal with, but when you have a bunch that you need to reference in different parts of the stack, that pattern starts to get real ugly. Luckily, `cats-effect` offers us a convenient way around this problem as well. It comes in two parts.

### Stacks of resources

The first part is that for a long-lived resource (like a database), anything that uses that resource becomes a resource itself, all the way up the call stack. For example:

```scala
import cats.syntax.traverse.* // for the `sequence` call
import mouse.all.* // for the `|>` pipe operator 

class DB

object DB {
    def get(): Resource[IO, DB] = ???
}

class CrudService(db: DB)

object CrudService {
    def get(dbResource: Resource[IO, DB]): Resource[IO, CrudService] = 
        dbResource.map{ db => new CrudService(db) }
}

class CrudController(svc: CrudService)

object CrudController extends RestController {
    def get(svcResource: Resource[IO, CrudService]): Resource[IO, CrudController] =
        svcResource.map{ svc => new CrudController(svc) }
}

class Server(controllers: List[RestController])

object Server {
    def get(resources: List[Resource[IO, RestController]]): Resource[IO, Server] = 
        resource.sequence.map{ controllers => new Server(controllers) }
}

object Main {
    val serverResource: Resource[IO, Server] = DB.get()
        |> CrudService.get
        |> CrudController.get
        |> List.apply
        |> Server.get

    def run(args: List[String]): IO[ExitCode] = {
        serverResource.useForever.as(ExitCode.Success)
    }

}
```

We can condense some of that using the dependency injection tools detailed [here](dependency-injection.md), such that the main object can be simplified:

```scala
import similarity.inject.{*, given}

object Main extends IOApp {
    val serverResource = injectResource[Server]

    def run(args: List[String]): IO[ExitCode] = {
        serverResource.useForever.as(ExitCode.Success)
    }
}
```

### ResourceApp

The second (smaller) piece is that instead of using `IOApp` for our `Main` object, we can use `cats-effect`'s `ResourceApp`. `ResourceApp` looks and works just like `IOApp`, but instead of the `run` function returning an `IO[ExitCode]`, it instead returns a `Resource[IO, ExitCode]` and `cats-effect` takes care of opening the resource, running the app, and closing the resource on application shutdown. So:

```scala
import cats.effect.ResourceApp

object Main extends ResourceApp {
    val serverResource = injectResource[Server]

    def run(args: List[String]): Resource[IO, ExitCode] = {
        for {
            result <- server.attempt // lifts any failure encountered at runtime into an `Either` so that we can see and handle it here
        } yield {
            result match {
                case Left(_)  => ExitCode.Error
                case Right(_) => ExitCode.Success
            }
        }
    }
}
```

And that's what our application looks like now:

```scala
// file src/main/scala/similarity/app/main.scala

object main extends ResourceApp {

  given Config = Config.load
  given ioRuntime: IORuntime = IORuntime.global

  /** Execution context used to provide asynchrony to the server below
    */
  val ec: ExecutionContext = ioRuntime.compute
  val serverResource: Resource[IO, Server] = server(ec)

  /** Run the server, wrapped in [[cats.effect.Resource]].
    *
    * @param args
    *   Ignored
    * @return
    */
  override def run(args: List[String]): Resource[IO, ExitCode] = {
    logging.configure
    serverResource.attempt.map {
      _ match {
        case Left(_)  => ExitCode.Error
        case Right(_) => ExitCode.Success
      }
    }
  }
}
```
