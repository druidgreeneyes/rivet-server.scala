# Persistence

This application is designed to decouple application logic from persistence logic; as such, which persistence backend you choose to use can be configured in `src/main/resources/application.yml`:

```yaml
persistence:
  backend: MongoDB
```

As of this writing, the options are `HashMap` for simple in-memory storage and `MongoDB` to connect to a running MongoDB instance.

## Setup

In order to get the application environment ready for a given persistence provider, you will should be able to use the scripts in `sbin`; there should always be a `start-X.sh` script and a `X.env` script, which you should use in order before calling sbt:

```sh
$ sbin/start-mongodb.sh
5dc168b2-d530-4812-8249-67918ff64d7b
$ source sbin/mongodb.env
$ env | grep RIVET
RIVET_MONGODB_CONNECTION_URL=mongodb://127.0.0.1:27017/?serverSelectionTimeoutMS=2000
RIVET_MONGODB_DATABASE_NAME=rivet
$ sbt clean run
[...]
2023.05.11 20:41:31:115 io-compute-16 INFO org.http4s.blaze.channel.nio1.NIO1SocketServerGroup
    Service bound to address /127.0.0.1:8096
2023.05.11 20:41:31:147 blaze-acceptor-0-0 DEBUG org.http4s.blaze.channel.nio1.SelectorLoop
    Channel initialized.
2023.05.11 20:41:31:147 io-compute-16 INFO org.http4s.blaze.server.BlazeServerBuilder
    
      _   _   _        _ _
     | |_| |_| |_ _ __| | | ___
     | ' \  _|  _| '_ \_  _(_-<
     |_||_\__|\__| .__/ |_|/__/
                 |_|
2023.05.11 20:41:31:165 io-compute-16 INFO org.http4s.blaze.server.BlazeServerBuilder
    http4s v0.23.18 on blaze v0.23.14 started at http://127.0.0.1:8096/
2023.05.11 20:41.31.196 main INFO
    Using Persistence backend MongoDB
```

## Implementing a Persistence backend

To implement a new persistence backend (say, DynamoDB for example), start by creating a package in `similarity/persistence`

```
src/main/scala/similarity/persistence/dynamodb
+- package.scala
```

Then add your Repository type as an option in `similarity/persistence/backend`:

```scala
enum Backend {
  case HashMap extends Backend
  case MongoDB extends Backend
  case SQL extends Backend
  case DynamoDB extends Backend
}
```

And as a case in the main `persistence` package:

```scala
  object DocumentRepository {
    given injector[ID, E](using Config, IORuntime): Persistence[ID, E] = {
      val backend = inject[Config].persistence.backend
      log.info(s"Using Persistence Backend: ${backend}")
      backend match {
        case Backend.HashMap => HashMapRepository()
        case Backend.MongoDB => MongoDBRepository()
        case Backend.SQL     => SQLRepository()
        case Backend.DynamoDB => DynamoDBRepository() // <-- Doesn't exist yet, we'll create it in a minute
      }
    }
  }
```

Your repository implementation will need to satisfy the `DocumentRepository` trait. Assuming the names of some DynamoDB types:

```scala
package similarity.util.persistence

import inject.{*, given}
import com.aws.sdk.dynamodb.*
import cats.effect.IO
import cats.effect.kernel.Resource
import similarity.config.Config

package object dynamodb {
    // Constructor should be private because it should be inaccessible except as a Resource[IO, DocumentRepository]
    case class DynamoDBRepository private (private val conn: DynamoDBConnection) extends DocumentRepository {
        private val storage = conn.getDatabase("some-database-name")
        // Implement the rest of the interface
    }
}
```

Lastly, you will need an empty constructor that provides the repository as a resource, so that we can handle any allocation and cleanup of network or file resources (connection pools, etc)

```scala
package object dynamodb {
    object DynamoDBRepository {
        def apply()(using Config): Resource[IO, DynamoDBRepository] = {
          inject[Resource[IO, DynamoDBConnection]].map{conn => new DynamoDBRepository(conn)}
        }
    }
}
```

That's the simple case. In most cases you will probably need a way to translate between the data model class 'DocumentEntity' and whatever the backend uses:

```scala
package object dynamodb {
    def toEntity(doc: DynamoDBDocument): DocumentEntity = ???
    def toDocument(doc: DocumentEntity): DynamoDBDocument = ???

    case class DynamoDBPersistence(private val conn: DynamoDBConnection) extends DocumentRepository {
        private val storage = conn.getDatabase("some-database-name")
        override def get(id: ID): Option[E] = storage.get(id).map(toEntity)
        override def save(id: ID, thing: E): Option[E] = storage.put(id, toDocument(thing))
        override def delete(id: ID): Option[E] = storage.remove(id).map(toEntity)
    }
}
```

The client library you're using may not provide for just getting a connection `Resource`. In that case, there are a couple of easy ways to wrap it:

```scala
// If the connection implements Closeable:
Resource.fromAutoCloseable(IO { DynamoDBConnection.create(connectionUrl) })

// If not, you can just manually provide for allocation and cleanup.
// Resource.make takes one of each, respectively:
Resource.make(
    // Provide an IO for opening the connection
    IO { DynamoDBConnection.create(connectionUrl) }
  )(
    // Provide a Thing -> IO[Unit] for closing the connection
    conn => IO { conn.disconnectAll() }
  )
```
