val scala3Version = "3.2.2"
val tapirVersion = "1.4.0"
val http4sVersion = "0.23.14"
val fs2Version = "3.7.0"
val fs2dataVersion = "1.7.0"
val scribeVersion = "3.11.1"
val scalacheckeffectVersion = "1.0.4"
val doobieVersion = "1.0.0-RC1"

lazy val rivet = project
  .in(file("."))
  .enablePlugins(GitlabPlugin)
  .settings(
    name := "rivet-server",
    version := "0.1.0-SNAPSHOT",
    scalaVersion := scala3Version,
    semanticdbEnabled := true,
    gitlabRepositories ++= Seq(
      GitlabProjectRepository(gitlabDomain.value, GitlabProjectId("3605588")),
    ),
    scalacOptions ++= Seq("-feature", "-deprecation"),
    Test / parallelExecution := false,
    // Types and Syntax
    libraryDependencies ++= Seq(
      "org.typelevel" %% "mouse" % "1.2.1",
      "org.typelevel" %% "cats-core" % "2.9.0",
      "org.typelevel" %% "cats-effect" % "3.5.0",
      "co.fs2" %% "fs2-core" % fs2Version,
      "co.fs2" %% "fs2-io" % fs2Version,
      "co.fs2" %% "fs2-reactive-streams" % fs2Version,
    ),
    // Logging
    libraryDependencies ++= Seq(
      "com.outr" %% "scribe" % scribeVersion,
      "com.outr" %% "scribe-slf4j" % scribeVersion,
    ),
    // RIV
    libraryDependencies ++= Seq(
      "com.gitlab.druidgreeneyes" % "rivet-core" % "1.0.7-SNAPSHOT",
      "io.brunk.tokenizers" %% "tokenizers" % "0.0.2",
      "org.scala-lang.modules" %% "scala-parallel-collections" % "1.0.4",
    ),
    // Web Server
    libraryDependencies ++= Seq(
      "org.http4s" %% "http4s-blaze-server" % http4sVersion,
      "com.softwaremill.sttp.tapir" %% "tapir-core" % tapirVersion,
      "com.softwaremill.sttp.tapir" %% "tapir-http4s-server" % tapirVersion,
      "com.softwaremill.sttp.tapir" %% "tapir-json-circe" % tapirVersion,
    ),
    // Config
    libraryDependencies ++= Seq(
      "io.circe" %% "circe-yaml" % "0.14.2",
    ),
    // MongoDB
    libraryDependencies ++= Seq(
      ("org.mongodb.scala" %% "mongo-scala-driver" % "4.9.0")
        .cross(CrossVersion.for3Use2_13),
    ),
    // SQL DB
    libraryDependencies ++= Seq(
      "org.tpolecat" %% "doobie-core" % doobieVersion,
      "org.tpolecat" %% "doobie-hikari" % doobieVersion,
      "org.xerial" % "sqlite-jdbc" % "3.42.0.0",
    ),
    // File parsing
    libraryDependencies ++= Seq(
      "org.gnieh" %% "fs2-data-csv" % fs2dataVersion,
    ),
    // Testing
    libraryDependencies ++= Seq(
      "org.scalameta" %% "munit" % "0.7.29",
      "com.softwaremill.sttp.tapir" %% "tapir-sttp-stub-server" % tapirVersion,
      "com.softwaremill.sttp.client3" %% "fs2" % "3.8.15",
      "org.scalacheck" %% "scalacheck" % "1.17.0",
      "org.scalameta" %% "munit-scalacheck" % "0.7.29",
      "org.typelevel" %% "scalacheck-effect" % scalacheckeffectVersion,
      "org.typelevel" %% "scalacheck-effect-munit" % scalacheckeffectVersion,
      "org.typelevel" %% "munit-cats-effect-3" % "1.0.7",
    ).map { it => it % Test },
  )
