# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://vagrantcloud.com/search.
  config.vm.box = "debian/bullseye64"

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # NOTE: This will enable public access to the opened port
  # config.vm.network "forwarded_port", guest: 80, host: 8080

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine and only allow access
  # via 127.0.0.1 to disable public access
  # config.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "127.0.0.1"

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  # config.vm.network "private_network", ip: "192.168.33.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder ".", "/workspace"

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  config.vm.provider "virtualbox" do |vb|
    # Display the VirtualBox GUI when booting the machine
    vb.gui = true
  
    # Customize the amount of memory on the VM:
    vb.memory = "1024"

    # Customize vram
    vb.customize ["modifyvm", :id, "--vram", "24"]
  end
  #
  # View the documentation for the provider you are using for more
  # information on available options.

  # Enable provisioning with a shell script. Additional provisioners such as
  # Ansible, Chef, Docker, Puppet and Salt are also available. Please see the
  # documentation for more information about their specific syntax and use.
  config.vm.provision "shell" do |s|
    s.name = "base"
    s.inline = <<-SHELL
      export DEBIAN_FRONTEND=noninteractive
      # install curl, sdk needs
      echo "install curl, sdk needs..."
      echo ""
      apt-get -qq update && apt-get -qqy install curl zip unzip

      # install kde
      echo "install kde..."
      echo ""
      apt-get -qqy install kde-plasma-desktop firefox-esr xinit

      # add "vagrant" password to vagrant user
      echo "update vagrant user password for UI login..."
      echo ""
      echo "vagrant:vagrant" | chpasswd

      # install vbox guest addons
      echo "install virtualbox guest addons..."
      echo "..."
      apt-get -qq install dkms build-essential linux-headers-$(uname -r) libxt6 libxmu6
      wget https://download.virtualbox.org/virtualbox/7.0.8/VBoxGuestAdditions_7.0.8.iso
      sudo mkdir /media/VBoxGuestAdditions
      sudo mount -o loop,ro VBoxGuestAdditions_7.0.8.iso /media/VBoxGuestAdditions
      sudo sh /media/VBoxGuestAdditions/VBoxLinuxAdditions.run
      rm VBoxGuestAdditions_7.0.8.iso
      sudo umount /media/VBoxGuestAdditions
      sudo rmdir /media/VBoxGuestAdditions
      echo "Done!"
    SHELL
  end

  config.vm.provision "shell" do |s|
    s.name = "jvm"
    s.privileged = false
    s.inline = <<-SHELL    
      # install sdkman
      echo "Install sdkman..."
      echo ""
      curl -s "https://get.sdkman.io" | bash
      # verify sdkman
      source "/home/vagrant/.sdkman/bin/sdkman-init.sh"
      sdk version
      # install java 17 and sbt
      echo "Install jdk 17..."
      echo ""
      sdk install java 17.0.7-tem
      sdk default java 17.0.7-tem

      echo "Install sbt..."
      echo ""
      sdk install sbt
      # verify java and sbt
      java -version
      sbt version
    SHELL
  end
end
