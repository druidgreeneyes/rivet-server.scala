FROM gitpod/workspace-full

RUN curl -s "https://get.sdkman.io" | bash
RUN bash -c 'source "/home/gitpod/.sdkman/bin/sdkman-init.sh" \
  && sdk install java 17.0.7-tem \
  && sdk install sbt'
